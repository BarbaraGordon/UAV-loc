from ast import Break
import glob
from mimetypes import init
import os
import time
from turtle import distance
import warnings
import sys
from math import sqrt
import math
from cv2 import mean
import numpy as np
import matplotlib.pyplot as plt
from sympy import true
import torch
# from torch.autograd import Variable
from torchvision import transforms
from torchvision.transforms import Normalize
from torch.nn.functional import interpolate
import csv
import haversine as hs
from haversine import Unit

import deep_feat.DeepLKBatch as dlk
import utility.img_utils as img_utility
import utility.param_utils as param_utility
import utility.utils as utils
import utility.config as config
import deep_feat.GetPoseParam as GPP
# import map_search.map_search as map_search （retrieval-based localization 模块，还没有写出来）
# map_search 还没写出来
import superpoint.feature_match as feature_match

from typing import Literal

# 导入dino库
from dino.utilities import DinoV2ExtractFeatures    # 引用dinov2，原始代码来自Anyloc
import deep_feat.config as dlk_config
from DeepAerialMatching.util.parameter_transfer import theta_to_H, H_to_theta
from DeepAerialMatching.model.AerialNet import dinov2_net_single_stream as net
from DeepAerialMatching.geotnf.transformation import GeometricTnf, theta2homogeneous

from utility.utils import process_num
if process_num == 0:

    import utility.map_tile_process as map_tile
    from utility.map_tile_process import MAP_TILE_GEO_INFO, map_tile_info_list
    from utility.map_tile_process import map_tile_amount, map_tile_row_amount, map_tile_col_amount
    from utility.map_tile_process import Map_Tile_Info

if process_num == 1:
    import utility.map_tile_process_ as map_tile
    from utility.map_tile_process_ import MAP_TILE_GEO_INFO, map_tile_info_list
    from utility.map_tile_process_ import map_tile_amount, map_tile_row_amount, map_tile_col_amount
    from utility.map_tile_process_ import Map_Tile_Info

os.environ['CUDA_VISIBLE_DEVICES'] = '0'
USE_CUDA = torch.cuda.is_available()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
sys.path.insert(0, './deep_feat')   # for model loading

# REVIEW 邵星雨加
DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"
model_zoo = ['dlk', 'dinolk']
net_zoo = ['DLK', 'DeepAerialMatching']
choose_model = 0    # 0对应dlk，1对应dinolk
choose_net = 0      # 0对应DLK，1对应DeepAerialMatching
if choose_model != 0 and choose_model != 1:
    print('Model chosen ERROR!')
    sys.exit(1)
print(f'Chosen model: {model_zoo[choose_model]}')
if choose_net!=0 and choose_net!=1:
    print('Net chosen ERROR!')
    sys.exit(2)


if choose_model == 1:
    # config.img_height_opt_net = 280
    pass

# control the result visualize and record
show_fig = False            # 是否展示结果的图像
show_error = True           # 是否展示偏差
record_data = True * show_error
SAVE_WARP_FIG = False


def compute_Pmk(P_init, P, T):
    P_mk = np.zeros((T.shape[0], 8, 1))

    k_ind = 0
    H_init = param_utility.p_to_H(P_init)
    H_k = param_utility.p_to_H(P[0, :, :])

    H_mk_samp = np.concatenate((
        H_init,
        H_k
    ), axis=0)

    H_mk = np.linalg.inv(H_mk_samp)
    H_mk_mat = np.eye(3)

    for i in range(H_mk.shape[0]):
        H_mk_mat = np.dot(H_mk[i, :, :], H_mk_mat)

    H_mk_mat_samp = np.linalg.inv(H_mk_mat)
    P_mk[k_ind, :, :] = param_utility.H_to_p(H_mk_mat_samp)
    return P_mk


def extract_map_templates(P_mk, M_tens, T, img_c, img_h, img_w):

    # 这里的templates指的是什么？
    k_num = T.shape[0]
    _, _, map_h, map_w = M_tens.size()
    M_tmpl = np.zeros((k_num, img_c, img_h, img_w))

    if (map_w / map_h) > (img_w / img_h):
        aspect = img_w / img_h
        adj_img_w = round(aspect * map_h)
        left = round(map_w / 2 - adj_img_w / 2)
        upper = 0
        right = round(map_w / 2 + adj_img_w / 2)
        lower = map_h
    else:
        aspect = img_h / img_w
        adj_img_h = round(aspect * map_w)
        left = 0
        upper = round(map_h / 2 - adj_img_h / 2)
        right = map_w
        lower = round(map_h / 2 + adj_img_h / 2)

    k_ind = 0
    print('extracting map templates...')
    for k in T:
        P_mk_tens = torch.from_numpy(P_mk[k_ind: k_ind + 1, :, :]).float()
        P_mk_tens = P_mk_tens.to(device)
        M_warp_tens, _, xy_patch_curr_cor = dlk.warp_hmg_lite(M_tens, P_mk_tens, img_h, img_w)
        M_tmpl[k_ind, :, :, :] = M_warp_tens.cpu().squeeze()

        # print('... {:d}/{:d} ...'.format(k_ind + 1, k_num))
        k_ind = k_ind + 1

    return M_tmpl, xy_patch_curr_cor


def UAV_loc_sys(Init_UAV_info):

    # Initial State of UAV, estimate the initial homography
    Pre_UAV_info = Init_UAV_info    # Map_Search得到的初始值或者是config里设定的初始值

    # templates indices and visibility neighborhood define
    T = np.array([1])       # 这个T是什么？

    # For result record
    distance_error_list = []    # 记录误差：average localization error。具体是什么意思？
    diff_score_list = []
    running_time_list = []
    P_opt = []      # 用来存储P？

    loc_idx = 0     # record
    image_list = sorted(glob.glob(os.path.join(config.image_dir, config.image_dir_ext)))
    Pre_UAV_info['image_list'] = image_list
    I_prev_for_rel_loc, _ = img_utility.load_single_I(
        # image_list[Pre_UAV_info['image_idx']],
        Pre_UAV_info['curr_image'],
        # utility文件夹下函数
        config.img_height_rel_pose, None)   # 制备视觉里程计的图像

    # ts_prev = float((image_list[Pre_UAV_info['image_idx']]).split('@')[1])
    # lon_prev = float((image_list[Pre_UAV_info['image_idx']]).split('@')[2])
    # lat_prev = float((image_list[Pre_UAV_info['image_idx']]).split('@')[3])
    # # 这个数据集里没有高度，把高度设成0
    # # 原始的高度数值设置（altitude）：alt_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[4])
    # alt_curr = 0
    # GPS_prev = [lat_prev, lon_prev]     # 这是什么变量？


    # M = img_utility.load_M(config.map_loc)

    # # 改成载入小地图块——邵星雨
    # M = img_utility.load_M(map_loc)

    # # utility文件夹下函数
    # M_tens = torch.from_numpy(M).unsqueeze(0).float().to(device)
    # _, map_h, map_w = M.shape

    # # 对准初始值？
    # curr_P_init = param_utility.cal_P_init(Init_UAV_info, map_w, map_h)


    # # w和h指的是图片的hight和width _, map_h, map_w = M.shape
    # Init_UAV_info['is_located'] = True

    is_first_loop = True   # 邵星雨：初始化的时候才需要使用求init_P的函数，求过第一次之后就把这个参数设成零
    CSV_write_header = True

    while True:
        #  break if all of the image is processed
        if ~Init_UAV_info['is_located'] and Pre_UAV_info['image_idx'] > len(image_list) - 2:    # 这个条件是什么意思？
            break

        # 邵星雨——这里还需要重新选择地图，不能只在最开始进行判断。
        Pre_UAV_info, map_tile_index_trans = map_tile.choose_map_tile(Pre_UAV_info)
        lat_index_nearest = Pre_UAV_info['map_tile_index'][0]
        lon_index_nearest = Pre_UAV_info['map_tile_index'][1]
        map_loc = MAP_TILE_GEO_INFO[lat_index_nearest][lon_index_nearest].path
        
        print(f'loop {loc_idx + 1}')
        print(f'Map tile\'s path: {map_loc}')
        print(f'UAV Picture\'s path: {Pre_UAV_info['curr_image']}')

        if map_tile_index_trans == [0, 0]:
            map_tile_change_flag = 0
        else:
            map_tile_change_flag = 1

        # 改成载入小地图块——邵星雨
        M = img_utility.load_M(map_loc)

        M_tens = torch.from_numpy(M).unsqueeze(0).float().to(device)
        _, map_h, map_w = M.shape

        if is_first_loop:
            curr_P_init = param_utility.cal_P_init(Init_UAV_info, map_w, map_h)
            Init_UAV_info['is_located'] = True
            is_first_loop = False
        else:
            pass
        

        start_timestamp = time.time()

        ts_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[1])
        lon_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[2])
        lat_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[3])
        # 这个数据集里没有高度，把高度设成0
        # 原始的高度数值设置（altitude）：alt_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[4])
        try:
            alt_curr = float((image_list[Pre_UAV_info['image_idx'] + config.interval]).split('@')[4])
        except:
            alt_curr = None
        GPS_curr = [lat_curr, lon_curr] # 当前经纬度

        I_curr_for_rel_loc, I_curr_for_match_loc = img_utility.load_single_I(
            image_list[Pre_UAV_info['image_idx'] + config.interval],
            config.img_height_rel_pose, config.img_height_opt_net)
        # 这里是relevent和match，用来加载降采样的照片。
        img_c, img_h, img_w = np.shape(I_curr_for_match_loc) # type: ignore

        curr_P = param_utility.cal_P_rel(I_prev_for_rel_loc, I_curr_for_rel_loc, config.img_height_rel_pose)    # 求先后两个照片视觉里程计之间变化的P（相当于求ΔH），这里求ΔH的时候用到了机器学习superpoint模型。
        curr_P = param_utility.scale_P(curr_P, s=map_tile.scale_img_to_map) # 这里之前是config.scale_img_to_map


        # 邵星雨：换地图之后需要改curr_P_init
        if map_tile_change_flag == 1:   # 如果参考地图切片发生了变化
            curr_P_init_ = curr_P_init

            # 使用加法 1
            # 使用乘法 2
            method = 2  # TODO - 选定是用乘法进行坐标系变换还是用加法进行坐标系变换
            if method != 1 and method != 2:
                print('ERROR: 错误的坐标系转换方法！')
                sys.exit()

            # 计算坐标系平移的距离
            # 这里应该用像素数做平移的单位
            # 用像素需要知道裁剪的时候横向和纵向分别每次各移动多少像素

            # 坐标系之间的变化只有平移

            coordinate_trans_x = map_tile_index_trans[0] * map_tile.pixel_trans_height  # 这里是从前一张到后一张小地图平移的模量
            coordinate_trans_y = map_tile_index_trans[1] * map_tile.pixel_trans_width
            
            if method == 1:
                coordinate_trans_H = [0, 0, -coordinate_trans_y,
                                    0, 0, -coordinate_trans_x,
                                    0, 0, 0]
            
            # 这个是从第二张地图到第一张地图的变换矩阵
                H_standard_np_inv = np.array(coordinate_trans_H).reshape((3,3)) 

                H_standard_np_inv_ = np.array([H_standard_np_inv])

                # 不用乘法了，直接用加减法。
                curr_H_init_ = param_utility.p_to_H(curr_P_init_)
                curr_H_init = H_standard_np_inv_ + curr_H_init_     # 只有平移，应该可以直接相加
                curr_P_init = param_utility.H_to_p(curr_H_init)


            # 使用乘法
            if method == 2:

                coordinate_trans_H = [1, 0, -coordinate_trans_y,
                                    0, 1, -coordinate_trans_x,
                                    0, 0, 1]
                H_standard_np_inv = np.array(coordinate_trans_H).reshape((3,3)) 
                H_standard_np_inv_ = np.array([H_standard_np_inv])
                
                curr_H_init_ = param_utility.p_to_H(curr_P_init_)
                curr_H_init = H_standard_np_inv_ @ curr_H_init_     # REVIEW - 相乘不能使用*
                curr_P_init = param_utility.H_to_p(curr_H_init)
            

        P_mk = compute_Pmk(curr_P_init, curr_P, T)


        # cascade localization process
        for idx in range(1):        # idx是当前的对准次数，对准两次结果会更准
            # scale factor is a adjustable parameters for a better localization
            # P_mk = param_utility.adjust_P_mk(P_mk)
            P_mk = param_utility.scale_P_size(P_mk, s = 1)        # 把1.1改成1
            M_tmpl, xy_cor = extract_map_templates(P_mk, M_tens, T, img_c, img_h, img_w)
            if idx == 0:
                xy_cor_list.append(xy_cor[0])
                M_tmpl_init = M_tmpl

            T_np = np.expand_dims(I_curr_for_match_loc, axis=0) # type: ignore
            T_tens = torch.from_numpy(T_np).requires_grad_(False).float()
            T_tens_nmlz = dlk.normalize_img_batch(T_tens)

            M_tmpl_tens = torch.from_numpy(M_tmpl).requires_grad_(False).float()
            M_tmpl_tens_nmlz = dlk.normalize_img_batch(M_tmpl_tens)

            if choose_net == 0:
            # 这下面是在干什么？图像预处理：将图像归一化。
                
                if choose_model == 0:
                    ### with dlk alignment, the conv_flag means using the deep imagery representation or not
                    # dlk_net = dlk.DeepLK(dlk.custom_net(config.model_path))
                    dlk_encoder_net = dlk.vgg16Conv('./models/vgg16_model.pth').to(device)  # type: ignore # 实际使用的是
                    # 用于单应性矩阵估计，一个是从照片到地图的粗略矩阵估计，另一个是从照片到照片的视觉里程计的单应性估计。
                    dlk_encoder_net.load_state_dict(torch.load(config.model_path, map_location=device))    # 用到了the_best_val.pth
                    dlk_net = dlk.DeepLK(dlk_encoder_net).to('cpu')
                    p_lk, _, itr_dlk = dlk_net(M_tmpl_tens_nmlz, T_tens_nmlz, tol = 1e-5, max_itr = config.max_itr_dlk, conv_flag = 1, ret_itr = True)
                    # 这个dlk_net是什么用法？dlk_net相当于是神经网络本身的结构。这里相当于把两个图像放进去求P的差值，也就是相对位置变化。

                # SECTION
                # 这里加一个dino的求法。
                # '''

                # 待查询图像T numpy图像T_np   tensor张量T_tens
                # 裁切地图块M numpy图像M_tmpl tensor张量M_tmpl_tens
                else:
                    dlk_net = dlk.DeepLK(None).to('cpu')
                    desc_model: str = 'dinov2_vitg14'
                    # desc_model: str = 'dinov2_vits14'
                    desc_layer: int = 31  # ViT-G14 layer31
                    # desc_layer: int = 11    # ViT-S14 layer12
                    desc_dim: int = 1536
                    # desc_dim: int = 1536
                    desc_facet: Literal["query", "key", "value", "token"] = "value"
                    DEVICE = "cuda:0" if torch.cuda.is_available() else "cpu"
                    extractor = DinoV2ExtractFeatures(desc_model, desc_layer, desc_facet, device = DEVICE)
                    
                    h_new, w_new = (img_h // 14) * 14, (img_w // 14) * 14
                    reshape_h, reshape_w = h_new // 14, w_new // 14
                    config.img_height_opt_net = h_new
                    # numpy转PIL

                    # T_tens_nmlz_rs = T_tens_nmlz.resize((w_new, h_new)) # PIL的resize用不了
                    # T_tens_nmlz_rs = T_tens_nmlz[:, :, 0: h_new: 1, 0: w_new: 1]
                    T_tens_rs = interpolate(T_tens, (h_new, w_new))
                    T_tens_nmlz_rs = dlk.normalize_img_batch(T_tens_rs)
                    # M_tmpl_tens_nmlz_rs = M_tmpl_tens_nmlz.resize((w_new, h_new)) # PIL的resize用不了
                    # M_tmpl_tens_nmlz_rs = M_tmpl_tens_nmlz[:, :, 0: h_new: 1, 0: w_new: 1]
                    M_tmpl_tens_rs = interpolate(M_tmpl_tens, (h_new, w_new))
                    M_tmpl_tens_nmlz_rs = dlk.normalize_img_batch(M_tmpl_tens_rs)

                    T_ret = extractor(T_tens_nmlz_rs)
                    M_tmpl_ret = extractor(M_tmpl_tens_nmlz_rs)

                    dino_feat_T_ = T_ret.reshape(1, reshape_h, reshape_w, desc_dim)
                    dino_feat_T = torch.permute(dino_feat_T_, (0, 3, 1, 2))

                    dino_feat_M_tmpl_ = M_tmpl_ret.reshape(1, reshape_h, reshape_w, desc_dim)
                    dino_feat_M_tmpl = torch.permute(dino_feat_M_tmpl_, (0, 3, 1, 2))

                    # SECTION - 尝试画出这两个匹配的图像
                    dino_feat_T_np = dino_feat_T[0].cpu().detach().numpy().transpose(1, 2, 0)
                    dino_feat_M_tmpl_np = dino_feat_M_tmpl[0].cpu().detach().numpy().transpose(1, 2, 0)
                    dino_feat_T_np_resize = np.zeros((h_new, w_new))
                    dino_feat_M_tmpl_np_resize = np.zeros((h_new, w_new))
                    # for w_i in range(w_new):
                    #     for h_i in range(h_new):
                    #         dino_feat_T_np_resize[h_i*14:(h_i+1)*14, w_i*14:(w_i+1)*14] = dino_feat_T_np[h_i, w_i]
                    #         dino_feat_M_tmpl_np_resize[h_i*14:(h_i+1)*14, w_i*14:(w_i+1)*14] = dino_feat_M_tmpl_np[h_i, w_i]

                    # !SECTION

                    p_lk, _, itr_dlk = dlk_net(dino_feat_M_tmpl, dino_feat_T, tol = dlk_config.tol, max_itr = dlk_config.max_itr_dinolk, conv_flag = 0, ret_itr = True)
                    # NOTE 之前把dino_feat_M_tmpl, dino_feat_T的顺序放反了
                # '''
                # !SECTION


            else:
                use_cuda = torch.cuda.is_available()
                h_new, w_new = (img_h // 14) * 14, (img_w // 14) * 14
                reshape_h, reshape_w = h_new // 14, w_new // 14
                config.img_height_opt_net = h_new

                T_tens_rs = interpolate(T_tens, (h_new, w_new))
                M_tmpl_tens_rs = interpolate(M_tmpl_tens, (h_new, w_new))
                dinov2_model_name = 'dinov2_vitb14'
                model = net(geometric_model='affine',dinov2_model_name=dinov2_model_name,input_size=(h_new, w_new))
                resize = GeometricTnf(out_h=h_new, out_w=w_new, use_cuda=False)
                normalizeTnf = Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                T_tens_rs_nmlz = normalizeTnf(T_tens_rs)
                M_tmpl_tens_rs_nmlz = normalizeTnf(M_tmpl_tens_rs)
                batch = {'source_image': T_tens_rs_nmlz, 'target_image':M_tmpl_tens_rs_nmlz}
                affTnf = GeometricTnf(geometric_model='affine', out_h=h_new, out_w=w_new, use_cuda=use_cuda)
                theta_aff, theta_aff_inv = model(batch)
                batch_size = theta_aff.size(0)
                theta_aff_inv = theta_aff_inv.view(-1, 2, 3)    # 变成(batch_size,2,3)的仿射参数矩阵，加一行[0, 0, 1]就是单应矩阵
                theta_aff_inv = torch.cat((theta_aff_inv, (torch.Tensor([0, 0, 1]).to('cuda').unsqueeze(0).unsqueeze(1).expand(batch_size, 1, 3))), 1)
                theta_aff_2 = theta_aff_inv.inverse().contiguous().view(-1, 9)[:, :6]

                theta_aff_ensemble = (theta_aff + theta_aff_2) / 2  # Ensemble
                H_lk = theta2homogeneous(theta_aff_ensemble)
                p_lk = param_utility.H_to_p(H_lk)

            # # try image transform method
            # M_tmpl_transf_tens = img_transf.transform(M_tmpl, save_fig=True)
            # M_tmpl_transf_tens_nmlz = dlk.normalize_img_batch(M_tmpl_transf_tens)
            # T_transf_tens = img_transf.transform(I_curr_for_match_loc, save_fig=True)
            # T_transf_tens_nmlz = dlk.normalize_img_batch(T_transf_tens)
            # dlk_net = dlk.DeepLK(dlk.custom_net(config.model_path))
            # p_lk, _, itr_dlk = dlk_net(M_tmpl_transf_tens_nmlz, T_transf_tens_nmlz, tol = 1e-5, max_itr = config.max_itr_dlk, conv_flag = 1, ret_itr = True)

            ### try with superpoint
            # M_tmpl_spp, _ = extract_map_templates(P_mk, M_tens, T, img_c, img_h*6, img_w*6)
            # p_found = feature_match.superpoint_single_match(M_tmpl_spp.squeeze(), I_curr_for_rel_loc)
            # spp_scale = config.img_height_opt_net / config.img_height_rel_pose
            # p_found = param_utility.scale_P(p_found, s = spp_scale)
            # p_lk = torch.from_numpy(p_found).reshape(1, 8, 1)

            # # try with sift
            # p_lk = GPP.get_param(M_tmpl_tens_nmlz, T_tens_nmlz, config.img_height_rel_pose)

            p_lk = p_lk.cpu()
            p_lk_np = param_utility.scale_P(p_lk.data.numpy(), 1)
            diff_score = torch.sqrt(torch.sum(torch.pow(M_tmpl_tens_nmlz - T_tens_nmlz, 2))).item()     # 这个量是什么？
            diff_score = diff_score / (M_tmpl_tens.shape[1] * M_tmpl_tens.shape[2] * M_tmpl_tens.shape[3])
            # 计算定位误差。
            print("the photometric error of two feature maps: ", diff_score)
            diff_score_list.append(diff_score)

            # # without the refine process of image-to-map alignment
            # p_lk_np = np.zeros([1, 8, 1])

            s_sm = (1 * config.img_height_opt_net) / map_h
            curr_P_init_scale = param_utility.scale_P(curr_P_init, s_sm)    # 这里如果用dino的话需要根据14的倍数的高度修改config.img_height_opt_net
            curr_P_scale = param_utility.scale_P(curr_P, s_sm)

            H_rel_samp = param_utility.p_to_H(p_lk_np)
            H_org_samp = param_utility.p_to_H(curr_P_scale)
            H_opt_samp = H_org_samp @ H_rel_samp
            # 粗定位H乘精定位H

            P_opt_i_scale = param_utility.H_to_p(H_opt_samp)
            s_lg = map_h / (1 * config.img_height_opt_net)
            P_opt_i = param_utility.scale_P(P_opt_i_scale, s_lg)
            P_opt.append(P_opt_i)

            H_init_samp = param_utility.p_to_H(curr_P_init)
            H_opt_i_samp = param_utility.p_to_H(P_opt_i)
            H_init_samp_new = H_init_samp @ H_opt_i_samp
            curr_P_init = param_utility.H_to_p(H_init_samp_new)

            P_mk = curr_P_init
            curr_P = torch.zeros([1, 8, 1])

        # update the current state of the UAV
        Curr_UAV_info = param_utility.extract_P_info(P_mk, Pre_UAV_info, map_h, map_w)
        Curr_UAV_info['image_idx'] = Curr_UAV_info['image_idx'] + config.interval
        Curr_UAV_info['curr_image'] = image_list[Curr_UAV_info['image_idx']]

        if not os.path.exists(f'./result/{config.test_dataset}/{map_tile.result_save_idx}/{model_zoo[choose_model]}/'):
            os.makedirs(f'./result/{config.test_dataset}/{map_tile.result_save_idx}/{model_zoo[choose_model]}/')

        if show_error:
            # for visualization, considering the time consume, it can be noted
            P_mk_opt = compute_Pmk(curr_P_init_scale, P_opt_i_scale, T)
            P_mk0 = compute_Pmk(curr_P_init_scale, curr_P_scale, T)
            H_mk_samp = param_utility.p_to_H(P_mk_opt)
            H_mk0_samp = param_utility.p_to_H(P_mk0)
            H_mk_rel_samp = np.linalg.inv(H_mk0_samp) @ H_mk_samp
            P_mk_rel_samp = param_utility.H_to_p(H_mk_rel_samp)

            targ_line = np.array([[0, 0], [M_tmpl_tens.shape[3] - 1, M_tmpl_tens.shape[2] - 1]])

            plt.subplot(3, 1, 1)
            plt.title('M{:d}'.format(loc_idx + 1))
            # M_tmpl_init_tens = Variable(torch.from_numpy(M_tmpl_init).float())
            M_tmpl_init_tens = torch.from_numpy(M_tmpl_init).float()
            if Curr_UAV_info['is_located']:
                plt.imshow(img_utility.plt_axis_match_np(M_tmpl_init_tens[0, :, :, :]))
            else:
                plt.imshow(np.zeros(((M_tmpl_init_tens.shape)[2], (M_tmpl_init_tens.shape)[3], 3)))
            plt.plot(targ_line[:, 0], targ_line[:, 1], 'r-')
            plt.plot(round(M_tmpl_tens.shape[3] / 2), round(M_tmpl_tens.shape[2] / 2), 'ro')
            plt.axis('off')

            plt.subplot(3, 1, 2)
            plt.title('M{:d} Warp'.format(loc_idx + 1))
            plt.axis('off')

            # show warp
            M_tmpl_curr_tens = M_tmpl_tens.float()
            P_mk_rel_samp_curr = torch.from_numpy(param_utility.scale_P(P_mk_rel_samp, 1)).float()
            M_tmpl_w, _, _ = dlk.warp_hmg_lite(M_tmpl_curr_tens, P_mk_rel_samp_curr, img_h, img_w)
            _, _, xy_cor_curr_opt = dlk.warp_hmg_lite(M_tens, torch.from_numpy(P_mk).float(), img_h, img_w)
            # without warp
            # we suppose [x-->img_h] [y-->img_w], and the output is [y,x]
            # M_tmpl_w, _, xy_cor_curr_opt = dlk.warp_hmg_lite(M_tens, torch.from_numpy(P_mk).float(), img_h, img_w)
            if Curr_UAV_info['is_located']:
                plt.imshow(img_utility.plt_axis_match_tens(M_tmpl_w[0, :, :, :]))
            else:
                plt.imshow(np.zeros(((M_tmpl_w.shape)[2], (M_tmpl_w.shape)[3], 3)))
            plt.plot(targ_line[:, 0], targ_line[:, 1], 'r-')
            plt.plot(round(M_tmpl_tens.shape[3] / 2), round(M_tmpl_tens.shape[2] / 2), 'ro')
            plt.axis('off')

            plt.subplot(3, 1, 3)
            plt.imshow(img_utility.plt_axis_match_np(I_curr_for_match_loc))
            plt.plot(targ_line[:, 0], targ_line[:, 1], 'r-')
            plt.plot(round(M_tmpl_tens.shape[3] / 2), round(M_tmpl_tens.shape[2] / 2), 'ro')
            plt.title('I{:d}'.format(loc_idx + 1))
            plt.axis('off')

            if show_fig:
                plt.axis('off')
                plt.cla()
                plt.savefig('./result/{}/{}/{}/warp_{}.png'.format(config.test_dataset, map_tile.result_save_idx, model_zoo[choose_model], Pre_UAV_info['image_idx']), dpi=1000, bbox_inches='tight', pad_inches=0.0)
                plt.show()
                plt.cla()

        end_timestamp = time.time()
        print("the runtime of localization : ", end_timestamp - start_timestamp)
        running_time_list.append(end_timestamp - start_timestamp)
        print('finished iteration: {:d} \n'.format(loc_idx + 1))
        if show_error:
            if SAVE_WARP_FIG:
                plt.savefig('./result/{}/{}/{}/warp_{}.png'.format(config.test_dataset, map_tile.result_save_idx, model_zoo[choose_model], Pre_UAV_info['image_idx']), dpi=1000, bbox_inches='tight', pad_inches=0.01)

        # if not os.path.exists(f'./result/{config.test_dataset}/{map_tile.result_save_idx}/{model_zoo[choose_model]}'):
        #     os.makedirs(f'./result/{config.test_dataset}/{map_tile.result_save_idx}/{model_zoo[choose_model]}')

        # if show_fig:
        #     plt.savefig('./result/{}/{}/warp_{}.png'.format(config.test_dataset, model_zoo[choose_model], Pre_UAV_info['image_idx']), dpi=1000, bbox_inches='tight', pad_inches=0.0)
        #     plt.show()
        #     plt.cla()

        if Curr_UAV_info['is_located']:
            if show_error:
                # print the localization result
                print("rough coordinate: ", xy_cor[0])
                xy_patch_org_cor_opt_ = dlk.warp_hmg_Noncentric(M, P_mk, xy_cor_curr_opt[0], img_w=T_tens.shape[3],
                                                                img_h=T_tens.shape[2])
                # the output is [y,x]
                xy_patch_org_cor_opt = xy_cor_curr_opt[0]
                xy_gps_curr_opt = utils.pix_pos_to_lat_lon(Pre_UAV_info['map_tile_index'], xy_patch_org_cor_opt)    # 这里原来第一个参数是M
                xy_cor_list_opt.append(xy_cor_curr_opt[0])
                xy_gps_list_opt.append(xy_gps_curr_opt)
                print("optimized coordinate: ", xy_cor_curr_opt[0])
                GPS_curr_pred = utils.pix_pos_to_lat_lon(Pre_UAV_info['map_tile_index'], xy_cor_curr_opt[0])    # 这里原来第一个参数是M
                Pre_UAV_info['curr_GPS'] = GPS_curr_pred    # 这里用来存这个无人机在世界坐标系下的经纬度，（lon，lat）——邵星雨
                xy_cor_curr_gt = utils.lat_lon_to_pix_pos(Pre_UAV_info['map_tile_index'], GPS_curr)  # [y,x]    # 这里原来第一个参数是M
                xy_cor_list_gt.append(xy_cor_curr_gt)
                print("true coordinate: ", [xy_cor_curr_gt[0], xy_cor_curr_gt[1]])
                # distance_error_curr = config.map_resolution * sqrt(
                #     (xy_cor_curr_gt[0] - xy_patch_org_cor_opt[0]) ** 2 + (xy_cor_curr_gt[1] - xy_patch_org_cor_opt[1]) ** 2)
                # 改成小地图——邵星雨
                distance_error_curr = map_tile.map_tile_resolution * sqrt(
                    (xy_cor_curr_gt[0] - xy_patch_org_cor_opt[0]) ** 2 + (xy_cor_curr_gt[1] - xy_patch_org_cor_opt[1]) ** 2)
                print("localization error: ", distance_error_curr, "m")
                distance_error_list.append(distance_error_curr)
                loc_img_list.append(Pre_UAV_info['curr_image'])
                # preparation of the next localization
                loc_idx += 1
                I_prev_for_rel_loc = I_curr_for_rel_loc
                Pre_UAV_info = Curr_UAV_info
        else:
            print("wrong when UAV localization in ", image_list[Curr_UAV_info['image_idx']])
            Pre_UAV_info['is_located'] = False
            break

        # if successful, show the map and record data
        if show_fig:
            xy_cor_curr_square = param_utility.corner_calculate(param_utility.p_to_H(P_mk), map_h, map_w, img_h, img_w)
            xy_cor_curr_square = xy_cor[1:]
            xy_cor_curr_square = xy_cor_curr_opt[1:]
            targ_line = np.array([
                [xy_cor_curr_square[0][0], xy_cor_curr_square[0][1]],
                [xy_cor_curr_square[1][0], xy_cor_curr_square[1][1]],
                [xy_cor_curr_square[2][0], xy_cor_curr_square[2][1]],
                [xy_cor_curr_square[3][0], xy_cor_curr_square[3][1]],
                [xy_cor_curr_square[0][0], xy_cor_curr_square[0][1]],
            ])

            # M_pil_marked = img_utility.Add_M_Markers_list(M, xy_cor_list, color="red")
            # M_pil_marked = transforms.ToPILImage()(torch.from_numpy(M_pil_marked))
            # M_pil_marked = img_utility.Add_M_Markers_square(M_pil_marked, xy_cor_curr_square, color="green")
            plt.figure()
            plt.axis('off')
            plt.title('Warp Square')
            xy_cor_list_np = np.array(xy_cor_list)
            xy_cor_list_gt_np = np.array(xy_cor_list_gt)
            plt.plot(xy_cor_list_np[:, 0], xy_cor_list_np[:, 1], "-+", linewidth=1, color="r", ms=0.5)
            plt.plot(xy_cor_list_gt_np[:, 0], xy_cor_list_gt_np[:, 1], "-*", linewidth=1, color="g", ms=0.5)
            plt.plot(targ_line[:, 0], targ_line[:, 1], 'r-')
            plt.imshow(transforms.ToPILImage()(torch.from_numpy(M)))
            plt.savefig('./result/{}/{}/{}/result_{}.png'.format(config.test_dataset, map_tile.result_save_idx, model_zoo[choose_model], Pre_UAV_info['image_idx']), dpi=500, bbox_inches='tight',
                        pad_inches=0.0)
            plt.cla()
            plt.show()
            plt.cla()

        if record_data:
            header = ('时间戳', '实际经度', '实际纬度', '预测经度', '预测纬度', '实际高度(m)', '预测高度(m)', '地理定位误差(m)', '定位时长(s)', '标准定位误差')
            if CSV_write_header == True:
                with open('./result/{}/{}/{}/result_{}.csv'.format(config.test_dataset, map_tile.result_save_idx, model_zoo[choose_model], config.satellite_map_name), 'w', encoding='UTF8', newline='') as f:
                    pass
            with open('./result/{}/{}/{}/result_{}.csv'.format(config.test_dataset, map_tile.result_save_idx, model_zoo[choose_model], config.satellite_map_name), 'a', encoding='UTF8', newline='') as f: # type: ignore
                writer = csv.writer(f)
                loc1 = (lat_curr, lon_curr)
                loc2 = (GPS_curr_pred[1], GPS_curr_pred[0])
                altitude_pred = Curr_UAV_info['altitude']
                # two ways to calculate the localization error
                dist_error = hs.haversine(loc1, loc2, unit=Unit.METERS)
                if CSV_write_header == True:
                    writer.writerow(header)
                    CSV_write_header = False
                print(alt_curr, altitude_pred)
                line = [ts_curr, lon_curr, lat_curr, GPS_curr_pred[0], GPS_curr_pred[1], alt_curr, altitude_pred, dist_error, end_timestamp - start_timestamp, diff_score]
                writer.writerow(line)
            # break the localization loop if there is bigger localization error than a set threshold
            if dist_error > 99999999:
                break

    print("average localization error: ", np.mean(np.array(distance_error_list)))
    print("average localization runtime: ", np.mean(np.array(running_time_list)[1:]))
    return Pre_UAV_info


if __name__ == "__main__":
    xy_cor_list = []
    xy_gps_list_opt = []
    xy_cor_list_opt = []
    xy_cor_list_gt = []
    loc_img_list = []

    # setting parameters for showing the result
    warnings.filterwarnings("ignore")           # 无视报警
    plt.rcParams['font.sans-serif'] = ['Tahoma']
    plt.rcParams['axes.unicode_minus'] = False
    # plt.ion()
    # plt.ioff()
    plt.figure(figsize=(10, 5))
    np.set_printoptions(precision=4)

    # parameters setting of localization
    # we can set the initial position of UAV if it is available
    Init_UAV_info = {
        'image_list': None,
        'trans_x': config.init_x,       # config在utility下面
        'trans_y': config.init_y,
        'angle': config.init_angle,
        # 'scale': config.init_scale,
        'scale': map_tile.initial_scale,
        'altitude': None,
        'curr_image': None,
        'curr_GPS': None,
        'is_located': False,
        'image_idx': 0,
        'map_tile_index':[-1, -1],
    }
    print("Testing image directory is : ", os.path.join(config.image_dir, config.image_dir_ext))
    image_list = sorted(glob.glob(os.path.join(config.image_dir, config.image_dir_ext)))
    # https://www.runoob.com/python/python-func-sorted.html
    # https://blog.csdn.net/qq_36201400/article/details/108745107
    Init_UAV_info['image_idx'] = config.image_idx
    Init_UAV_info['curr_image'] = image_list[Init_UAV_info['image_idx']]

    # start localization process  开始定位过程
    Curr_UAV_info = Init_UAV_info
    while True:
        start_ts = time.time()
        # initialization of localization
        # Curr_UAV_info = map_search.map_search(Curr_UAV_info)
        # print("initialization of localization spends: ", time.time() - timestamp_1)

        # sequential localization
        Curr_UAV_info = UAV_loc_sys(Curr_UAV_info)
        print("Total spent time: ", time.time() - start_ts)