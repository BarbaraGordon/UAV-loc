from cv2 import line
import matplotlib.pyplot as plt
import matplotlib
import csv
# 设置显示中文字体
matplotlib.rcParams["font.sans-serif"] = ["SimSun"]

dist_error = []
diff_score = []
true_lon = []
true_lat = []
cal_lon = []
cal_lat = []
cal_time = []
a = ''
# with open(r".\result\Village\wm_w_h_stride~~300_600_600_1-4\dlk\result_village.csv", "r", encoding='utf-8') as f:
# with open(r".\result\Village\wm_w_h_stride~~300_600_600_1-4\dinolk\result_village.csv", "r", encoding='utf-8') as f:
#     a = 'village'
# with open(r".\result\Gravel_Pit\wm_w_h_stride~~300_600_600_1-4\dlk\result_gravel-pit.csv", "r", encoding='utf-8') as f:
with open(r".\result\Gravel_Pit\wm_w_h_stride~~300_600_600_1-4\dinolk\result_gravel-pit.csv", "r", encoding='utf-8') as f:
#     a = 'gravel-pit'
# with open(r".\result/JimoFlight_LT/wm_w_h_stride~~300_600_600_1-4/dlk/result_jimo_2023-03-18-15-40-18.csv", "r", encoding='utf-8') as f:
# with open(r".\result/JimoFlight_LT/wm_w_h_stride~~300_600_600_1-4/dinolk/result_jimo_2023-03-18-15-40-18.csv", "r", encoding='utf-8') as f:
#     a = 'JimoFlight_LT'
    reader = csv.reader(f)
    data_num = -1
    for line in reader:
        if data_num == -1:
            data_num += 1
            continue
        dist_error_curr = float(line[-3])
        diff_score_curr = float(line[-1])
        dist_error.append(dist_error_curr)
        diff_score.append(diff_score_curr)
        true_lon.append(float(line[1]))
        true_lat.append(float(line[2]))
        cal_lon.append(float(line[3]))
        cal_lat.append(float(line[4]))
        cal_time.append(float(line[-2]))
        data_num += 1
count = range(data_num)

fig, ax = plt.subplots()
# ax.yaxis.tick_right()
ax.invert_xaxis()
plt.plot(true_lon, true_lat, label='True Location 真实位置', marker='+')
plt.plot(cal_lon, cal_lat, label='Calculated Location 计算位置', marker='+')
plt.legend()
plt.xlabel('Longitude 经度')
plt.ylabel('Latitude 纬度')
plt.title('定位结果（东半球北半球）')
plt.grid(axis='both')
plt.show()
pass
# plt.savefig(f'{a}定位结果.png',bbox_inches = 'tight', pad_inches = 1, dpi = 1500)

# fig, ax1 = plt.subplots()
# ax2 = ax1.twinx()
# ax1.plot(count, dist_error, label='Geographic Distance Error 地理定位误差(m)', color='r', marker='o')
# # ,linewidth=3,color='r',marker='o',markerfacecolor='blue',markersize=12
# ax2.plot(count, diff_score, label='Standard Deviation 标准定位误差', color='b', marker='+', linestyle='--')
# ax1.set_xlabel('Loop Order')
# ax1.set_ylabel('地理定位误差(m)',color = 'r')   #设置Y1轴标题
# ax2.set_ylabel('标准定位误差',color = 'b')   #设置Y2轴标题
# ax1.legend(loc='upper left')
# ax2.legend(loc='upper right')
# plt.title('定位误差曲线')
# # plt.legend(loc='upper left', bbox_to_anchor=(0.0, 1.0))
# plt.grid(True)
# plt.show()
# pass
# plt.savefig(f'{a}定位误差曲线.png',bbox_inches = 'tight', pad_inches = 1, dpi = 1500)

# plt.plot(count, cal_time, label='Time 计算时长(s)', marker='o')
# plt.xlabel('Loop Order')
# plt.ylabel('Time 计算时长(s)')
# plt.title('定位计算时间')
# # plt.legend()
# plt.grid(True)
# plt.show()
# pass
# plt.savefig(f'{a}定位计算时间.png',bbox_inches = 'tight', pad_inches = 1, dpi = 1500)