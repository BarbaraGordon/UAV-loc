# 测试的数据集
# test_dataset = "JimoFlight_LT"
# test_dataset = "Village"
# test_dataset = "Gravel_Pit"
test_dataset = "Qingdao_flight"

# general parameters
tol = 1e-3
max_itr = 10
lam1 = 1
lam2 = 0.01
scaling_for_disp = 1    # orginal 2.25
max_itr_dlk = 150


if test_dataset == "haidian_airport":
    # 海淀机场飞行
    # TODO: 修改图片序列所在文件夹
    image_dir = "..\\haidian_airport\\camera1\\"
    image_dir_ext = "*.png"
    satellite_map_name = 'haidian_airport'
    heading_dct = 'east'
    # map_loc = "F:\\SourceCode\\gps-denied-localization\\haidian_airport\\map_airport.jpg"
    map_loc = "..\\haidian_airport\\map_airport.tif"
    model_path = "../models/conv_02_17_18_1833.pth"
    opt_param_save_loc = "../haidian_airport/test_out.mat"
    image_idx = 30  # 文件夹中的初始索引
    interval = 5  # 加载图片用到的参数

    # TODO: 修改瓦片图库所在文件夹
    dataset_root_dir = 'F:\\Dataset\\USGS\\EDGE\\png_file\\wukuangguanshan\\MapTile'
    index_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\haidian_airport_index'
    query_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\haidian_airport_query'
    result_save_folder = '..\\map_search\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\search_test_query.ini'

    img_height_opt_net = 100
    img_height_rel_pose = 600
    map_resolution = 0.465
    # TODO: 这个参数需要对照所制备出的基准图进行修改，修改形式==>基准图图像高/img_height_rel_pose，如果不改变数据集，即可不改变
    scale_img_to_map = 2861 / img_height_rel_pose

    # 运行的时候的参数
    init_x = 3780  # TODO: 初始帧的像素x坐标，通过经纬度与基准图进行转化，对于当前的基准图，其北方在图像左侧，故使用纬度转化
    init_y = 1800  # TODO: 初始帧的像素y坐标，通过经纬度与基准图进行转化，对于当前的基准图，其北方在图像左侧，故使用经度转化
    init_scale = 1 / 6  # TODO: 实时图的像素所代表真实地理长度 / 基准图的像素所代表真实地理长度，当前设备条件下为1/6
    init_angle = 0  # TODO: 当前航向与图像短边所代表方向之间的夹角（角度制），这里图像长边为北方，因此为与北方的夹角，顺时钟为正

    # init_x = 2800
    # init_y = 1900

    Map_GPS_1 = [116.116820683836, 40.0856287594884]
    Map_GPS_2 = [116.10104906676413, 40.0652127940419]


elif test_dataset == "haidian_airport_IR":
    # 海淀机场飞行
    # TODO: 修改图片序列所在文件夹
    image_dir = "F:\\SourceCode\\gps-denied-localization\\haidian_airport\\IR_image1\\"
    image_dir_ext = "*.jpg"
    satellite_map_name = 'haidian_airport_IR'
    # map_loc = "F:\\SourceCode\\gps-denied-localization\\haidian_airport\\map_airport.jpg"
    map_loc = "..\\haidian_airport\\map_airport.tif"
    model_path = "../models/conv_02_17_18_1833.pth"
    opt_param_save_loc = "../haidian_airport/test_out.mat"
    image_idx = 0  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    dataset_root_dir = 'F:\\Dataset\\USGS\\EDGE\\png_file\\wukuangguanshan\\MapTile'
    index_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\haidian_airport_index'
    query_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\haidian_airport_query'
    result_save_folder = '..\\map_search\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\search_test_query.ini'

    img_height_opt_net = 100
    img_height_rel_pose = 600
    map_resolution = 0.465
    # TODO: 这个参数需要对照所制备出的基准图进行修改，修改形式==>基准图图像高/img_height_rel_pose，如果不改变数据集，即可不改变
    scale_img_to_map = 2861 / img_height_rel_pose

    # 运行的时候的参数
    init_x = 3780  # TODO: 初始帧的像素x坐标，通过经纬度与基准图进行转化，对于当前的基准图，其北方在图像左侧，故使用纬度转化
    init_y = 1800  # TODO: 初始帧的像素y坐标，通过经纬度与基准图进行转化，对于当前的基准图，其北方在图像左侧，故使用经度转化
    init_scale = 1 / 6  # TODO: 实时图的像素所代表真实地理长度 / 基准图的像素所代表真实地理长度，当前设备条件下为1/6
    init_angle = 0  # TODO: 当前航向与图像短边所代表方向之间的夹角（角度制），这里图像长边为北方，因此为与北方的夹角，顺时钟为正

    # init_x = 2800
    # init_y = 1900

    Map_GPS_1 = [116.116820683836, 40.0856287594884]
    Map_GPS_2 = [116.10104906676413, 40.0652127940419]

elif test_dataset == "huanbaoyuan":
    image_dir = "F:\\Dataset\\huanbaoyuan\\frames\\"
    image_dir_ext = "*.jpg"
    satellite_map_name = 'huanbaoyuan'
    # map_loc = "F:\\SourceCode\\gps-denied-localization\\haidian_airport\\map_airport.jpg"
    map_loc = "F:\\Dataset\\huanbaoyuan\\qiyuan.tif"
    model_path = "../models/conv_02_17_18_1833.pth"
    opt_param_save_loc = "../haidian_airport/test_out.mat"
    image_idx = 30  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    dataset_root_dir = 'F:\\Dataset\\huanbaoyuan\\MapTile'
    index_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_index'
    query_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_query'
    result_save_folder = '..\\map_search\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\huanbaoyuan_index.ini'

    img_height_opt_net = 100
    img_height_rel_pose = 600
    map_resolution = 0.950
    scale_img_to_map = 2870 / img_height_rel_pose

    # 运行的时候的参数
    init_x = 3150
    init_y = 900
    init_scale = 1 / 10
    init_angle = 0

    # init_x = 2800
    # init_y = 1900

    Map_GPS_1 = [116.20045234453252, 40.0674319456717]
    Map_GPS_2 = [116.14680897892245, 40.04289606485084]


elif test_dataset == "Jimo":
    # Chicago Dataset
    image_dir = "I:\\Learning_File\\SLAM\\SourceCode\\gps-denied-uav-localization - tiny\\jimo\\frames\\cam0\\frames\\"
    image_dir_ext = "*.jpg"
    satellite_map_name = 'jimo'
    map_loc = "../jimo/map_in.jpg"
    model_path = "../models/conv_02_17_18_1833.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = "../jimo/test_out.mat"
    map_resolution = 0.50
    scale_img_to_map = 2748 / img_height_rel_pose

    init_x = 2700
    init_y = 2200
    init_scale = 1 / 2.5
    init_angle = 0

    GPS_1 = [36.603175, 120.44791944]
    GPS_2 = [36.599975, 120.44145282]

    device = "cpu"
    USE_CUDA = False

# 只需要考虑这个
elif test_dataset == "JimoFlight_LT":
    # Chicago Dataset
    # 原始路径：image_dir = "G:\\NEW_DATASET_2023_03\\2023-03-18-15-40-18_full\\"
    image_dir = "E:\\GeoVINS\\AerialVL\\images\\VAL\\long_trajtr\\2023-03-18-15-40-18\\"
    # image_downsampling
    image_dir_ext = "*.png"
    satellite_map_name = 'jimo_2023-03-18-15-40-18'
    map_loc = "E:\\GeoVINS\\AerialVL\\images\\VAL\\geo_referenced_map\\@map@120.42114259488751@36.604504047017464@120.48431398161503@36.573629616877625@.tif"
    heading_dct = 'north'

    map_width = 5888
    map_height = 3584

    # model_path = "./models/conv_02_17_18_1833.pth"
    model_path = "./models/best_dlk_model.pth"

    image_idx = 400     # 文件夹中的初始索引
    interval = 1        # 加载图片用到的参数
    img_height_opt_net = 100    # 在照片与地图块比较的时候将地图块和照片的高度都缩小为100像素，因为这个过程要求的精度要低一些。
    img_height_rel_pose = 600   # 这个数据有什么物理意义？在两个照片进行视觉里程计计算时将高度都缩小为600像素。
    opt_param_save_loc = "../jimo/test_out.mat"

    map_resolution = 0.958
    
    # scale_img_to_map = map_height / img_height_rel_pose

    # the initial position if given by the GNSS data
    init_x = None
    init_y = None
    init_scale = 1 / 20
    init_angle = 90

    # init_angle = 0      # 地图切片的旋转角度是0，相对原始大地图旋转了90度

    Map_GPS_1 = [120.42114259488751, 36.604504047017464]
    Map_GPS_2 = [120.48431398161503, 36.573629616877625]

    device = "cpu"
    USE_CUDA = False


elif test_dataset == "Qingdao_simulate":
    image_dir = "F:\\Dataset\\Qingdao_simulate\\frames\\"
    image_dir_ext = "*.png"
    satellite_map_name = 'Qingdao_simulate'
    # map_loc = "F:\\SourceCode\\gps-denied-localization\\haidian_airport\\map_airport.jpg"
    map_loc = "F:\\Dataset\\Qingdao_simulate\\map_2021_2.jpg"
    model_path = "../models/conv_02_17_18_1833.pth"
    opt_param_save_loc = "../haidian_airport/test_out.mat"
    image_idx = 2000  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    dataset_root_dir = 'F:\\Dataset\\huanbaoyuan\\MapTile'
    index_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_index'
    query_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_query'
    result_save_folder = '..\\map_search\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\huanbaoyuan_index.ini'

    img_height_opt_net = 100
    img_height_rel_pose = 600
    map_resolution = 0.950
    scale_img_to_map = 3123 / img_height_rel_pose

    # 运行的时候的参数
    init_x = 1400
    init_y = 500
    init_scale = 1 / 10
    init_angle = -90

    # init_x = 2800
    # init_y = 1900

    Map_GPS_1 = [116.20045234453252, 40.0674319456717]
    Map_GPS_2 = [116.14680897892245, 40.04289606485084]


elif test_dataset == "Qingdao_flight":

    # Chicago Dataset
    # satellite_map_name = 'jimo_2023-03-18-15-40-18'

    # the initial position if given by the GNSS data

    image_dir = "E:\\GeoVINS\\AerialVL\\images\\VAL\\long_trajtr\\2023-03-18-15-01-14\\"
    image_dir_ext = "*.png"
    satellite_map_name = 'Qingdao_flight'
    map_loc = "E:\\GeoVINS\\AerialVL\\images\\VAL\\geo_referenced_map\\@map@120.42114259488751@36.604504047017464@120.48431398161503@36.573629616877625@.tif"
    model_path = "./models/best_dlk_model.pth"
    opt_param_save_loc = "../Qingdao_flight/test_out.mat"
    image_idx = 0  # 文件夹中的初始索引
    interval = 10  # 加载图片用到的参数
    heading_dct = 'north'
    map_width = 5888
    map_height = 3584

    # REVIEW - 贺梦凡原始
    # dataset_root_dir = 'F:\\Dataset\\huanbaoyuan\\MapTile'
    # index_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_index'
    # query_input_features_dir = '..\\map_search\\patchnetvlad\\output_features\\huanbaoyuan_query'
    # result_save_folder = '..\\map_search\\patchnetvlad\\output_features\\result'
    # config_path = '..\\map_search\\patchnetvlad\\configs\\huanbaoyuan_index.ini'

    img_height_opt_net = 100
    img_height_rel_pose = 600
    map_resolution = 0.958
    scale_img_to_map = map_height / img_height_rel_pose

    # 运行的时候的参数
    init_x = None
    init_y = None
    init_scale = 1 / 22
    init_angle = 90

    device = "cpu"
    USE_CUDA = False

    Map_GPS_1 = [120.42114259488751, 36.604504047017464]
    Map_GPS_2 = [120.48431398161503, 36.573629616877625]


elif test_dataset == "Village":
    # Village Dataset
    image_dir = "E:\\GeoVINS\\Open-Dataset\\village\\frames\\"
    image_dir_ext = "*.JPG"
    satellite_map_name = 'village'
    map_loc = "E:\\GeoVINS\\Open-Dataset\\village\\map_village.jpg"
    heading_dct = 'north'
    # model_path = "../models/conv_02_17_18_1833.pth"
    model_path = "./models/best_dlk_model.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = ".../village/test_out.mat"
    map_resolution = 0.45
    # scale_img_to_map = 2861 / img_height_rel_pose
    image_idx = 0  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    # REVIEW - 贺梦凡原始
    # dataset_root_dir = 'F:\\Dataset\\Drone\\Village\\MapTileSearch\\MapTile'
    # index_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_index'
    # query_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_query'
    # result_save_folder = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\result'
    # config_path = '..\\map_search\\patchnetvlad\\configs\\SenseFly_Village.ini'

    init_x = 2837
    init_y = 841
    # init_x = 0
    # init_y = 0
    init_scale = 1 / 6
    init_angle = - 135

    # REVIEW - 邵星雨
    # init_angle = -225

    Map_GPS_1 = [8.39368611111111, 47.0705666666667]
    Map_GPS_2 = [8.42165833333333, 47.0591972222222]

elif test_dataset == "Industrial":
    # Village Dataset
    image_dir = "H:\\Dataset\\SenseFly\\Industrial_estate\\thm_route_1\\"
    image_dir_ext = "*.JPG"
    satellite_map_name = 'Industrial'
    map_loc = "H:\\Dataset\\SenseFly\\Industrial_estate\\industrial_estate.jpg"
    model_path = "../models/conv_02_17_18_1833.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = "../Industrial/test_out.mat"
    map_resolution = 0.45
    scale_img_to_map = 4743 / img_height_rel_pose
    image_idx = 0  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    dataset_root_dir = 'F:\\Dataset\\Drone\\Village\\MapTileSearch\\MapTile'
    index_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_index'
    query_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_query'
    result_save_folder = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\SenseFly_Village.ini'

    # init_x = 2747
    # init_y = 924
    init_x = 5800
    init_y = 4100
    init_scale = 1 / 5
    init_angle = -75

    Map_GPS_1 = [47.0705666666667, 8.42165833333333]
    Map_GPS_2 = [47.0591972222222, 8.39368611111111]

elif test_dataset == "Village_2":
    # Village Dataset
    image_dir = "F:\\SourceCode\\UAV_loc_in_syst\\village\\frames_with_geotag_2\\"
    image_dir_ext = "*.JPG"
    satellite_map_name = 'village'
    map_loc = "../village/map_village.jpg"
    heading_dct = 'north'
    model_path = "../models/conv_02_17_18_1833.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = "../village/test_out.mat"
    map_resolution = 0.45
    scale_img_to_map = 2861 / img_height_rel_pose
    image_idx = 0  # 文件夹中的初始索引
    interval = 1  # 加载图片用到的参数

    dataset_root_dir = 'F:\\Dataset\\Drone\\Village\\MapTileSearch\\MapTile_2'
    index_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_index_2'
    query_input_features_dir = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\SenseFly_Village_query_2'
    result_save_folder = 'F:\\SourceCode\\Patch-NetVLAD\\patchnetvlad\\output_features\\result'
    config_path = '..\\map_search\\patchnetvlad\\configs\\SenseFly_Village_2.ini'

    init_x = 1260
    init_y = 2248
    init_scale = 1 / 6
    init_angle = 25

    Map_GPS_1 = [8.39368611111111, 47.0705666666667]
    Map_GPS_2 = [8.42165833333333, 47.0591972222222]

elif test_dataset == "Gravel_Pit":
    # Gravel-Pit Dataset
    image_dir = "E:\\GeoVINS\\Open-Dataset\\gravel-pit\\frames\\"
    image_dir_ext = "*.JPG"
    satellite_map_name = 'gravel-pit'
    map_loc = "E:\\GeoVINS\\Open-Dataset\\gravel-pit\\map_gravel_pit.jpg"
    # model_path = "../models/conv_02_17_18_1833.pth"
    model_path = "./models/best_dlk_model.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = "E:\\GeoVINS\\Open-Dataset\\gravel_pit\\test_out.mat"
    map_resolution = 0.32
    # scale_img_to_map = 2861 / img_height_rel_pose
    image_idx = 0  # 文件夹中的初始索引
    interval = 1

    init_x = 2500
    init_y = 1050
    init_scale = 1 / 4.3

    init_angle = -70
    # REVIEW 加了旋转之后
    # init_angle = -160

    image_idx = 0

    # Map_GPS_1 = [47.1148611111111, 7.91988055555556]  # NOTE - 最原始的版本
    # Map_GPS_2 = [47.1093700000000, 7.90523333333333]

    # Map_GPS_1 = [47.1093700000000, 7.91988055555556]
    # Map_GPS_2 = [47.1148611111111, 7.90523333333333]

    Map_GPS_1 = [7.90523333333333, 47.1148611111111]  # NOTE - 改一下顺序，应该lon在前lat在后
    Map_GPS_2 = [7.91988055555556, 47.1093700000000]



elif test_dataset == "Zeche":
    # Chicago Dataset
    image_dir = "../Zeche/frames/seq3/"
    image_dir_ext = "*.JPG"
    satellite_map_name = 'Zeche'
    map_loc = "../Zeche/Zeche_map.jpg"
    model_path = "../models/conv_02_17_18_1833.pth"
    img_height_opt_net = 100
    img_height_rel_pose = 600
    opt_param_save_loc = "../jimo/test_out.mat"
    map_resolution = 0.1
    scale_img_to_map = 3123 / img_height_rel_pose
    image_idx = 0
    interval = 1

    init_x = 780
    init_y = 400
    init_scale = 1 / 7.5
    init_angle = -150

    Map_GPS_1 = [36.603175, 120.44791944]
    Map_GPS_2 = [36.599975, 120.44145282]

else:
    raise NameError('Can\'t the dataset')