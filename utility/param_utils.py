import numpy as np
import torch
from torch.autograd import Variable
import sys
import math
import time

import deep_feat.GetPoseParam as GPP
import utility.utils as utils
import utility.config as config

from utility.utils import process_num
if process_num == 0:
    import utility.map_tile_process as map_tile
else:
    import utility.map_tile_process_ as map_tile


def p_to_H(p):
    if len(p.shape) < 3:
        p = np.expand_dims(p, 0)
    batch_sz, _, _ = p.shape # type: ignore
    batch_one = np.zeros((batch_sz, 1, 1))
    p_ = np.concatenate((p, batch_one), axis=1)
    iden = np.tile(np.eye(3), (batch_sz, 1, 1))
    H = np.reshape(p_, (batch_sz, 3, 3)) + iden

    return H


def H_to_p(H):
    if len(H.shape) < 3:
        H = np.expand_dims(H, 0)
        # 齐次坐标对应三维矩阵
    batch_sz, _, _ = H.shape # type: ignore
    H = H / np.reshape(H[:, 2, 2], (-1, 1, 1))
    iden = np.tile(np.eye(3), (batch_sz, 1, 1))
    p = np.reshape(H - iden, (batch_sz, 9, 1))
    p = p[:, 0:-1, :]

    return p


def scale_P(P, s):
    # for translation
    S = np.array([[s, 0, 0], [0, s, 0], [0, 0, 1]])
    S_tile = np.tile(S, (P.shape[0], 1, 1))
    H_unscale = p_to_H(P)
    H_scale = S_tile @ H_unscale @ np.linalg.inv(S_tile)
    P_scale = H_to_p(H_scale)

    return P_scale

def scale_H(H, scale):
    S = np.array([[scale, 0, 0], [0, scale, 0], [0, 0, 1]])
    H_scale = S @ H @ np.linalg.inv(S)
    return H_scale


def scale_P_size(P, s):
    ## for scale
    S = np.array([[s, 0, 0], [0, s, 0], [0, 0, s]])
    S_tile = np.tile(S, (P.shape[0], 1, 1))
    S_inv = np.array([[1, 0, 0], [0, 1, 0], [0, 0, s]])
    S_inv_tile = np.tile(S_inv, (P.shape[0], 1, 1))
    H_unscale = p_to_H(P)
    H_scale = S_tile @ H_unscale @ np.linalg.inv(S_inv_tile)    # @是什么运算？
    P_scale = H_to_p(H_scale)

    return P_scale


def extract_R_t(H):
    H_mk = H.copy()
    H_mk_np = np.array(H_mk).squeeze()
    trans_x = H_mk_np[0, 2]
    trans_y = H_mk_np[1, 2]
    angle_cos_1 = H_mk_np[0, 0]
    angle_sin_1 = -H_mk_np[0, 1]
    angle_sin_2 = H_mk_np[1, 0]
    angle_cos_2 = H_mk_np[1, 1]

    angle_1 = math.atan(angle_sin_1 / angle_cos_1)
    angle_2 = math.atan(angle_sin_1 / angle_cos_2)
    angle_3 = math.atan(angle_sin_2 / angle_cos_1)
    angle_4 = math.atan(angle_sin_2 / angle_cos_2)
    angle = (angle_1 + angle_2 + angle_3 + angle_4) / 4

    # math.atan \in [-math.pi/2, math.pi/2]
    # angle = angle * 180 / math.pi
    if angle_sin_1 < 0 and angle_sin_2 < 0 and angle > 0:
        angle -= math.pi
    elif angle_sin_1 > 0 and angle_sin_2 > 0 and angle < 0:
        angle += math.pi

    scale = math.sqrt((angle_cos_1 ** 2 + angle_cos_1 ** 2 + angle_cos_1 ** 2 + angle_cos_1 ** 2) / 2)

    return angle, scale, trans_x, trans_y


def cal_P_rel(I_1, I_2, img_h_rel_pose):
    template_batch = Variable(torch.from_numpy(I_1).squeeze(0))
    img_batch = Variable(torch.from_numpy(I_2).squeeze(0))
    start_timestamp = time.time()
    # this part spends most of the time in the localization process
    # tmp = GPP.get_param(img_batch, template_batch, img_h_rel_pose)
    tmp = GPP.get_param_spp(img_batch, template_batch, img_h_rel_pose)      # 这一部用到了机器学习superpoint模型。
    print("the runtime for loading image : {}".format(time.time() - start_timestamp))
    p_inv = np.array(tmp)
    H_inv = p_to_H(p_inv)
    H_rel = np.linalg.inv(H_inv)
    p_rel = H_to_p(H_rel)

    return p_rel


def cal_P_init(UAV_info, w, h):
    # 干什么的？没明白P和H换来换去在干什么。

    map_tile_index = UAV_info['map_tile_index']
    if UAV_info['trans_x'] is None or UAV_info['trans_y'] is None or ~UAV_info['is_located']:   # 确定是否是首次定位
        lon_init = float((UAV_info['image_list'][UAV_info['image_idx']]).split('@')[2])     # 如果是首次定位则直接使用图片标注的经纬度
        lat_init = float((UAV_info['image_list'][UAV_info['image_idx']]).split('@')[3])
        init_y, init_x = utils.lat_lon_to_pix_pos(map_tile_index, [lat_init, lon_init])   # 将照片所在的经纬度转化为像素坐标
        # 上面的式子原来函数的第一个参数是None
        # GPS_target:[lat_init, lon_init]
        
    else:
        init_x = UAV_info['trans_x']
        init_y = UAV_info['trans_y']
    angle_ = UAV_info['angle']
    scale = UAV_info['scale']

    # 这里正北方向上的似乎可以很好的符合
    t_x = float(init_x) - h /2  # 求照片中心点对应的地图的像素坐标
    t_y = float(init_y) - w /2
    angle = (angle_ / 180) * math.pi
    H_standard_inv = [scale * math.cos(angle), -scale * math.sin(angle), t_y,
                        scale * math.sin(angle), scale * math.cos(angle), t_x,
                        0, 0, 1]        # 标准单应性矩阵，初始照片对大地图的变化关系
    H_standard_np_inv = np.array(H_standard_inv).reshape((3,3))
    # H_standard_np = np.linalg.inv(H_standard_np_inv)    # 求逆
    # P_standard_np = np.reshape(H_standard_np - np.eye(3), (9, 1))
    # P_standard = np.expand_dims(P_standard_np[:8], 0)

    # H_all_inv = p_to_H(P_standard)
    # H_all = np.linalg.inv(H_all_inv)

    # REVIEW - 邵星雨
    H_all = np.array([H_standard_np_inv])
    
    P_init = H_to_p(H_all)

    return P_init


def cal_altitude(P_mk, map_h, map_w):
    fu, fv, cu, cv = [1195.0521207906766, 1182.9630662142304, 1002.6733832886946, 750.2384365311531]        # NOTE - 相机参数（邵星雨）
    u_FOV = 2 * math.atan(cu / fu)
    v_FOV = 2 * math.atan(cv / fv)

    raw_edge_points_left = np.array([[0, i, 1] for i in range(map_h)])
    raw_edge_points_right = np.array([[map_w, i, 1] for i in range(map_h)])
    raw_edge_points_top = np.array([[i, 0, 1] for i in range(map_h)])
    raw_edge_points_bottom = np.array([[i, map_h, 1] for i in range(map_h)])
    H_mk = p_to_H(P_mk)
    new_edge_points_left = np.dot(H_mk[0, :, :], raw_edge_points_left.transpose()).transpose()
    new_edge_points_left = new_edge_points_left / np.expand_dims(new_edge_points_left[:, 2], 1).repeat(3, axis=1)
    new_edge_points_right = np.dot(H_mk[0, :, :], raw_edge_points_right.transpose()).transpose()
    new_edge_points_right = new_edge_points_right / np.expand_dims(new_edge_points_right[:, 2], 1).repeat(3, axis = 1)
    new_edge_points_top = np.dot(H_mk[0, :, :], raw_edge_points_top.transpose()).transpose()
    new_edge_points_top = new_edge_points_top / np.expand_dims(new_edge_points_top[:, 2], 1).repeat(3, axis = 1)
    new_edge_points_bottom = np.dot(H_mk[0, :, :], raw_edge_points_bottom.transpose()).transpose()
    new_edge_points_bottom = new_edge_points_bottom / np.expand_dims(new_edge_points_bottom[:, 2], 1).repeat(3, axis = 1)

    # map_resolution = config.map_resolution

    map_resolution = map_tile.map_tile_resolution   # 换成小地图的分辨率——邵星雨

    cal_u = np.sqrt((new_edge_points_right - new_edge_points_left)[:, 0]**2+(new_edge_points_right - new_edge_points_left)[:, 1]**2).mean() * map_resolution
    cal_v = np.sqrt((new_edge_points_top - new_edge_points_bottom)[:, 0]**2+(new_edge_points_top - new_edge_points_bottom)[:, 1]**2).mean() * map_resolution

    h_v = cal_v / ( 2 * math.tan(v_FOV / 2) )
    h_u = cal_u / ( 2 * math.tan(u_FOV / 2) )

    return (h_v + h_u) / 2


def corner_track(H_p, map_h, map_w, scale):
    # 给出Corner track函数
    corners = np.array([[-map_w/2, map_w/2, -map_w/2, map_w/2],
                        [map_h / 2, map_h / 2, -map_h / 2, -map_h / 2],
                        [1, 1, 1, 1]])
    scale = 1/10
    img_h = round(map_h * scale)
    img_w = round(map_w * scale)
    corners_w_p_ = np.dot(H_p, corners)
    scale_factor = 1 / corners_w_p_[2,:]
    corners_w_p = corners_w_p_ * scale_factor

    # ## 本质是检验这里是不是一个矩形
    corner_tracker_1 = np.sqrt(np.sum((corners_w_p[0:2, 0] - corners_w_p[0:2, 1])**2))
    corner_tracker_2 = np.sqrt(np.sum((corners_w_p[0:2, 2] - corners_w_p[0:2, 3])**2))
    corner_tracker_3 = np.sqrt(np.sum((corners_w_p[0:2, 0] - corners_w_p[0:2, 2])**2))
    corner_tracker_4 = np.sqrt(np.sum((corners_w_p[0:2, 1] - corners_w_p[0:2, 3])**2))

    corner_vertor_1 = (corners_w_p[0:2, 0] - corners_w_p[0:2, 1])
    corner_vertor_2 = (corners_w_p[0:2, 2] - corners_w_p[0:2, 3])
    corner_vertor_3 = (corners_w_p[0:2, 0] - corners_w_p[0:2, 2])
    corner_vertor_4 = (corners_w_p[0:2, 1] - corners_w_p[0:2, 3])

    corner_angle_1_cos = np.dot(corner_vertor_1, corner_vertor_3) / (
                np.sqrt(np.sum(corner_vertor_1 ** 2)) * np.sqrt(np.sum(corner_vertor_3 ** 2)))
    corner_angle_1_sin = np.cross(corner_vertor_1, corner_vertor_3) / (
                np.sqrt(np.sum(corner_vertor_1 ** 2)) * np.sqrt(np.sum(corner_vertor_3 ** 2)))
    corner_angle_1 = np.arctan2(corner_angle_1_sin, corner_angle_1_cos) * 180 / np.pi

    corner_angle_2_cos = np.dot(corner_vertor_2, corner_vertor_4) / (
                np.sqrt(np.sum(corner_vertor_2 ** 2)) * np.sqrt(np.sum(corner_vertor_4 ** 2)))
    corner_angle_2_sin = np.cross(corner_vertor_2, corner_vertor_4) / (
                np.sqrt(np.sum(corner_vertor_2 ** 2)) * np.sqrt(np.sum(corner_vertor_4 ** 2)))
    corner_angle_2 = np.arctan2(corner_angle_2_sin, corner_angle_2_cos) * 180 / np.pi

    corner_angle_3_cos = np.dot(corner_vertor_1, corner_vertor_4) / (
            np.sqrt(np.sum(corner_vertor_1 ** 2)) * np.sqrt(np.sum(corner_vertor_4 ** 2)))
    corner_angle_3_sin = np.cross(corner_vertor_1, corner_vertor_4) / (
            np.sqrt(np.sum(corner_vertor_1 ** 2)) * np.sqrt(np.sum(corner_vertor_4 ** 2)))
    corner_angle_3 = np.arctan2(corner_angle_3_sin, corner_angle_3_cos) * 180 / np.pi

    corner_angle_4_cos = np.dot(corner_vertor_2, corner_vertor_3) / (
            np.sqrt(np.sum(corner_vertor_2 ** 2)) * np.sqrt(np.sum(corner_vertor_3 ** 2)))
    corner_angle_4_sin = np.cross(corner_vertor_2, corner_vertor_3) / (
            np.sqrt(np.sum(corner_vertor_2 ** 2)) * np.sqrt(np.sum(corner_vertor_3 ** 2)))
    corner_angle_4 = np.arctan2(corner_angle_4_sin, corner_angle_4_cos) * 180 / np.pi

    if (corner_tracker_1 > 3*img_w) or (corner_tracker_1 < 0.3*img_w):
        return False

    if (corner_tracker_2 > 2*img_w) or (corner_tracker_2 < 0.3*img_w):
        return False

    if (corner_tracker_3 > 3*img_h) or (corner_tracker_3 < 0.3*img_h):
        return False

    if (corner_tracker_4 > 3*img_h) or (corner_tracker_4 < 0.3*img_h):
        return False

    # ## 检验长宽比如何
    if (corner_tracker_1+corner_tracker_2) / (corner_tracker_3+corner_tracker_4) > 3:
        return False
    if (corner_tracker_1+corner_tracker_2) / (corner_tracker_3+corner_tracker_4) < 0.3:
        return False

    # ## 检验夹角
    if abs(corner_angle_1) < 30 or abs(corner_angle_1) > 120:
        return False
    if abs(corner_angle_2) < 30 or abs(corner_angle_2) > 120:
        return False
    if abs(corner_angle_3) < 30 or abs(corner_angle_3) > 120:
        return False
    if abs(corner_angle_4) < 30 or abs(corner_angle_4) > 120:
        return False

    return True


def corner_calculate(H_p, map_h, map_w, img_h, img_w):
    # give the function of corner loss
    aspect = img_w / img_h
    margin = (map_w - map_h * aspect) / 2
    H_p = H_p.squeeze()

    corners = np.array([[-(map_h*aspect)/2 + margin, (map_h*aspect)/2 - margin, (map_h*aspect)/2 - margin, -(map_h*aspect)/2 + margin],
                            [map_h/2, map_h/2, -map_h/2, -map_h/2],
                            [1, 1, 1, 1]])
    corners_w_p_ = np.dot(H_p, corners)
    scale_factor = 1 / corners_w_p_[2, :]
    corners_w_p = corners_w_p_ * scale_factor
    add_factor = np.array([map_w/2, map_h/2])
    corner_1 = list(np.transpose(corners_w_p[0:2, 0]) + add_factor)
    corner_2 = list(np.transpose(corners_w_p[0:2, 1]) + add_factor)
    corner_3 = list(np.transpose(corners_w_p[0:2, 2]) + add_factor)
    corner_4 = list(np.transpose(corners_w_p[0:2, 3]) + add_factor)
    return corner_1, corner_2, corner_3, corner_4


def extract_P_info(P_mk, Pre_UAV_info, map_h, map_w):
    """
    # the components of matrix H
    H_standard_inv = [scale * math.cos(angle), -scale * math.sin(angle), t_x,
                        scale * math.sin(angle), scale * math.cos(angle), t_y,
                        0, 0, 1]
    """
    H_mk = p_to_H(P_mk)
    H_mk_np = np.array(H_mk).squeeze()
    trans_x = H_mk_np[0, 2] + map_w / 2
    trans_y = H_mk_np[1, 2] + map_h / 2
    angle_cos_1 = H_mk_np[0, 0]
    angle_sin_1 = H_mk_np[0, 1]
    angle_cos_2 = H_mk_np[1, 0]
    angle_sin_2 = H_mk_np[1, 1]

    angle_1 = math.atan(-angle_sin_1/angle_cos_1)
    angle_2 = math.atan(-angle_sin_1/angle_cos_2)
    angle_3 = math.atan(angle_sin_2/angle_cos_1)
    angle_4 = math.atan(angle_sin_2/angle_cos_2)
    angle_np = np.array([angle_1, angle_2, angle_3, angle_4])
    angle_ = (angle_1 + angle_2 + angle_3 + angle_4)/4
    angle = angle_

    scale = math.sqrt((angle_cos_1 ** 2 + angle_cos_1 ** 2 + angle_cos_1 ** 2 + angle_cos_1 ** 2) / 2)

    is_located = corner_track(H_mk_np, map_h, map_w, scale)

    altitude = cal_altitude(P_mk, map_h, map_w)

    Pre_UAV_info['trans_x'] = trans_x
    Pre_UAV_info['trans_y'] = trans_y
    Pre_UAV_info['is_located'] = True
    # Pre_UAV_info['angle'] = angle
    # Pre_UAV_info['scale'] = scale
    Pre_UAV_info['altitude'] = altitude

    return Pre_UAV_info


def adjust_P_mk(P_mk):
    # the method to crop the map patch is so accurate that includes useless warp
    P_mk_copy = P_mk.copy()
    H_mk = p_to_H(P_mk)
    angle, scale, trans_x, trans_y = extract_R_t(H_mk)

    P_mk_copy[:, 0, :] = scale * math.cos(angle) - 1
    P_mk_copy[:, 4, :] = scale * math.cos(angle) - 1
    P_mk_copy[:, 1, :] = - scale * math.sin(angle)
    P_mk_copy[:, 3, :] = scale * math.sin(angle)

    return P_mk_copy


if __name__ == '__main__':
    pass
