import glob
from os import listdir
import os
import copy
import utility.config as config
import haversine
import numpy

class Map_Tile_Info:
    def __init__(self, path: str, lon_list: list[float], lat_list: list[float]):
        # 路径，lon经度（0,180），lat纬度（0,90）
        self.path = path       # 地图切片路径
        self.lon_range = lon_list      # 经纬度范围
        self.lat_range = lat_list
        self.lon_mid = (lon_list[0] + lon_list[1]) / 2    # 求中心点坐标
        self.lat_mid = (lat_list[0] + lat_list[1]) / 2

# @左上经度@左上纬度@右下经度@右下纬度
# 正好符合上北下南左西右东

# 地图切片的存储路径
map_tile_loc = 'E:\\GeoVINS\\AerialVL\\images\\VPR\\map_database\\180\\'

# 每个地图切片的像素宽度。每个地图切片都是正方形所以宽和高相等
map_tile_length = 500    # 单位是像素
# 切出来是棋盘格正方形

geo_trans_width = 50       # 单位是米
geo_trans_height = 50

map_path = config.map_loc
map_width = config.map_width
map_height = config.map_height
map_GPS_1 = config.Map_GPS_1
map_GPS_2 = config.Map_GPS_2
original_scale = config.init_scale
map_resolution = config.map_resolution

map_lon_1 = map_GPS_1[0]
map_lon_2 = map_GPS_2[0]
map_lat_1 = map_GPS_1[1]
map_lat_2 = map_GPS_2[1]
original_map_info = Map_Tile_Info(map_path, [map_lon_1, map_lon_2], [map_lat_1, map_lat_2])

# 这里应该加一个能够根据飞行高度切割地图块的函数
# 需要保证必须按照经线和纬线切，类似棋盘格
MAP_TILE_GEO_INFO = []      # 这里我想记录一个包含了地理相对位置的地图切片列表，能够找到某一图块东西南北相邻的地图切片
# map_tile_list = sorted(glob.glob(map_tile_loc))
map_tile_list = []
for filename in listdir(map_tile_loc):
    map_tile_list.append(map_tile_loc + filename)   # 得到绝对路径
map_tile_amount = len(map_tile_list)
flag_found_map_tile = 0
map_tile_info_list = []
for map_tile_i in range(map_tile_amount):
    map_tile_path = map_tile_list[map_tile_i]
    map_tile_lon_1 = float(map_tile_path.split('@')[2])
    map_tile_lat_1 = float(map_tile_path.split('@')[3])
    map_tile_lon_2 = float(map_tile_path.split('@')[4])
    map_tile_lat_2 = float(map_tile_path.split('@')[5])
    map_tile_lon = [map_tile_lon_1, map_tile_lon_2]     # longitude经度范围
    map_tile_lat = [map_tile_lat_1, map_tile_lat_2]     # latitude纬度范围

    map_tile_info = Map_Tile_Info(map_tile_path, map_tile_lon, map_tile_lat)
    # map_tile_info_list.append(map_tile_info)        # 这里append会覆盖前面所有元素！为啥？
    # map_tile_info_list.append(copy.deepcopy(map_tile_info))
    map_tile_info_list.append(copy.copy(map_tile_info))

map_tile_info_list_tmp = map_tile_info_list
map_tile_col_amount = 0 # 在地图中的row和col，每一行的经度是相同的
map_tile_info_list = sorted(map_tile_info_list_tmp, key = lambda info: (-info.lat_mid, info.lon_mid))
del map_tile_info_list_tmp
# 北半球纬度从上往下是逐渐变小的，所以加负号，北半球经度从左往右是逐渐变大的，让二维list里的上下左右符合地球、地图上的上下左右
common_lat = map_tile_info_list[0].lat_mid  # 记下第一个地图切片的中间点的经度，看每一个经度上有多少地图切片
for map_tile_i in range(map_tile_amount):
    if common_lat != map_tile_info_list[map_tile_i].lat_mid:
        map_tile_col_amount = map_tile_i
        break
map_tile_row_amount = round(map_tile_amount / map_tile_col_amount)
for i in range(map_tile_row_amount):
    MAP_TILE_GEO_INFO.append(map_tile_info_list[i * map_tile_col_amount: (i + 1) * map_tile_col_amount])
# 54行  87列
# 这里需要计算照片中的一个像素应该对应多少米，每次横向和纵向各移动50米

# 横向距离
if map_tile_row_amount % 2 == 0:    # 偶数列
    mid_1 = round(map_tile_row_amount / 2)
    mid_2 = mid_1 + 1
    map_tile_mid_1 = MAP_TILE_GEO_INFO[mid_1][0]
    map_tile_mid_2 = MAP_TILE_GEO_INFO[mid_2][0]
    lat_mid = (map_tile_mid_1.lat_mid + map_tile_mid_2.lat_mid) / 2

else:
    mid = round((map_tile_row_amount + 1) / 2)
    map_tile_mid = MAP_TILE_GEO_INFO[mid][0]
    lat_mid = map_tile_mid.lat_mid

w_1 = lat_mid
w_2 = MAP_TILE_GEO_INFO[0][0].lon_range
# (lat, lon)
width_coordinate_1 = (w_1, w_2[0])
width_coordinate_2 = (w_1, w_2[1])
map_tile_geo_width = haversine.haversine(width_coordinate_1, width_coordinate_2, haversine.Unit.METERS)

# 纵向距离
h_1 = MAP_TILE_GEO_INFO[0][0].lat_range
h_2 = MAP_TILE_GEO_INFO[0][0].lon_mid
# (lat, lon)
height_coordinate_1 = (h_1[0], h_2)
height_coordinate_2 = (h_1[1], h_2)
map_tile_geo_height = haversine.haversine(height_coordinate_1, height_coordinate_2, haversine.Unit.METERS)

map_tile_geo_length = (map_tile_geo_height + map_tile_geo_width) / 2        # 这里求个平均，理论上这个宽和高应该是相等的

# 求每张地图一个像素的宽度对应多少米

unit_pixel_geo_length = map_tile_geo_length / map_tile_length      # 地图切片中一个像素对应的地表的长度
pixel_trans_width = round(geo_trans_width / unit_pixel_geo_length)  # 求平移的像素数，不过目前横向和纵向平移的像素数应该是一样的。
pixel_trans_height = round(geo_trans_height / unit_pixel_geo_length)

map_tile_resolution = unit_pixel_geo_length                             
initial_scale = original_scale * map_resolution / map_tile_resolution   # 大地图scale/
# 照片分辨率/地图分辨率
initial_scale = original_scale * 19.07       # 乘19.07试试
# initial_scale = original_scale * 19.04       # 乘19.04试试

# 求一下大地图横向和纵向跨越的距离
# %% 横向距离（经度longitude跨度）
w_1 = original_map_info.lat_mid
w_2 = original_map_info.lon_range
# (lat, lon)
width_coordinate_1 = (w_1, w_2[0])
width_coordinate_2 = (w_1, w_2[1])

original_map_geo_width = haversine.haversine(width_coordinate_1, width_coordinate_2, haversine.Unit.METERS)

# %% 纵向距离（纬度latitude跨度）
h_1 = original_map_info.lat_range
h_2 = original_map_info.lon_mid
# (lat, lon)
height_coordinate_1 = (h_1[0], h_2)
height_coordinate_2 = (h_1[1], h_2)

original_map_geo_height = haversine.haversine(height_coordinate_1, height_coordinate_2, haversine.Unit.METERS)


def choose_map_tile(Pre_UAV_info: dict):
    map_tile_change_flag = 0

    if Pre_UAV_info['map_tile_index'] == [-1, -1]:# 只有初始化的时候才是[-1, -1]

        pic_lon_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[2])
        pic_lat_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[3])

        lon_length_list = []
        lat_length_list = []
        for col_i in range(map_tile_col_amount):
            map_tile_info = MAP_TILE_GEO_INFO[0][col_i]
            map_tile_lon_mid = map_tile_info.lon_mid
            lon_length = abs(map_tile_lon_mid - pic_lon_init)
            lon_length_list.append(lon_length)
        lon_index_nearest = lon_length_list.index(min(lon_length_list)) # 横向上找经度longitude最接近的列

        for row_i in range(map_tile_row_amount):
            map_tile_info = MAP_TILE_GEO_INFO[row_i][lon_index_nearest]
            map_tile_lat_mid = map_tile_info.lat_mid
            # lat_length = abs(map_tile_lat_mid - pic_lat_init)
            
            map_tile_lon_mid = map_tile_info.lon_mid
            point_1 = (map_tile_lat_mid, map_tile_lon_mid)
            point_2 = (pic_lat_init, pic_lon_init)
            lat_length = haversine.haversine(point_1, point_2, haversine.Unit.METERS)

            lat_length_list.append(lat_length)
        lat_index_nearest = lat_length_list.index(min(lat_length_list)) # 纵向上找纬度latitude最接近的列
        map_tile_index = [lat_index_nearest, lon_index_nearest]
        Pre_UAV_info['map_tile_index'] = map_tile_index
        
    else:   # 不是初始化的情况，也就是已经进入while true循环之后。
        
        # 需要将上一张定位的像素点位置转化为经纬度坐标？
        # 需要记录上一张图片的编号，下一次我们首次测试该编号的地图是否包含第二个图块进行检验；
        # 如果照片不在上一次使用的地图块范围内，则对这个图块四周的8个或者4个地图块进行测试，看这9个或5个地图块哪个最符合。
        pic_GPS = Pre_UAV_info['curr_GPS']
        pic_lon_init = pic_GPS[0]
        pic_lat_init = pic_GPS[1]
        pre_map_tile_index = Pre_UAV_info['map_tile_index']
        pre_map_tile_row = pre_map_tile_index[0]
        pre_map_tile_col = pre_map_tile_index[1]
        # pre_map_tile_info = MAP_TILE_GEO_INFO[pre_map_tile_row][pre_map_tile_col]
        # pre_map_tile_lat_1 = pre_map_tile_info.lat_range[0]
        # pre_map_tile_lat_2 = pre_map_tile_info.lat_range[1]
        # pre_map_tile_lon_1 = pre_map_tile_info.lon_range[0]
        # pre_map_tile_lon_2 = pre_map_tile_info.lon_range[1]
        # lat_flag = (pre_map_tile_lat_1 - pic_lat_init) * (pre_map_tile_lat_2 - pic_lat_init)
        # lon_flag = (pre_map_tile_lon_1 - pic_lon_init) * (pre_map_tile_lon_2 - pic_lon_init)
        # if lat_flag < 0 and lon_flag < 0:
        #     Pre_UAV_info['map_tile_index'] = pre_map_tile_index
        # else:
        #     lon_length_list = []
        #     lat_length_list = []
        #     delta = [-1, 0, 1]
        #     map_tile_row = delta + pre_map_tile_row
        #     map_tile_col = delta + pre_map_tile_col
        #     for i in map_tile_row:      # 确定排查的三排都在地图切片棋盘格之内
        #         if i >= map_tile_row_amount or i < 0:
        #             map_tile_row.remove(i)
        #     for i in map_tile_col:
        #         if i >= map_tile_col_amount or i < 0:
        #             map_tile_col.remove(i)

        #     for row_i in map_tile_row:
        #         map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
        #         map_tile_lon_mid = map_tile_info.lon_mid
        #         lon_length = abs(map_tile_lon_mid - pic_lon_init)
        #         lon_length_list.append(lon_length)
        #     lon_index_nearest = lon_length_list.index(min(lon_length_list))
        #     for col_i in map_tile_col:
        #         map_tile_info = MAP_TILE_GEO_INFO[lon_index_nearest][col_i]
        #         map_tile_lat_mid = map_tile_info.lat_mid
        #         lat_length = abs(map_tile_lat_mid - pic_lat_init)
        #         lat_length_list.append(lat_length)
        #     lat_index_nearest = lat_length_list.index(min(lat_length_list))
        #     curr_map_tile_index = [lat_index_nearest, lon_index_nearest]
        #     if curr_map_tile_index == pre_map_tile_index:
        #         Pre_UAV_info['map_tile_index'] = pre_map_tile_index
        #     else:
        #         Pre_UAV_info['map_tile_index'] = curr_map_tile_index
        #         map_tile_index_trans = curr_map_tile_index - pre_map_tile_index
        #         map_tile_change_flag = 1
        
        # 还是要全部地图都找一遍
        lon_length_list = []
        lat_length_list = []
        for col_i in range(map_tile_col_amount):
            map_tile_info = MAP_TILE_GEO_INFO[0][col_i]
            map_tile_lon_mid = map_tile_info.lon_mid
            lon_length = abs(map_tile_lon_mid - pic_lon_init)
            lon_length_list.append(lon_length)
        lon_index_nearest = lon_length_list.index(min(lon_length_list)) # 横向上找经度longitude最接近的列

        for row_i in range(map_tile_row_amount):
            map_tile_info = MAP_TILE_GEO_INFO[row_i][lon_index_nearest]
            map_tile_lat_mid = map_tile_info.lat_mid
            # lat_length = abs(map_tile_lat_mid - pic_lat_init)
            
            map_tile_lon_mid = map_tile_info.lon_mid
            point_1 = (map_tile_lat_mid, map_tile_lon_mid)
            point_2 = (pic_lat_init, pic_lon_init)
            lat_length = haversine.haversine(point_1, point_2, haversine.Unit.METERS)

            lat_length_list.append(lat_length)
        lat_index_nearest = lat_length_list.index(min(lat_length_list)) # 纵向上找纬度latitude最接近的列
        map_tile_index = [lat_index_nearest, lon_index_nearest]
        if map_tile_index == pre_map_tile_index:
            Pre_UAV_info['map_tile_index'] = pre_map_tile_index
        else:
            Pre_UAV_info['map_tile_index'] = map_tile_index
            map_tile_index_trans = (numpy.array(map_tile_index) - numpy.array(pre_map_tile_index)).tolist()
            map_tile_change_flag = 1
        pass
    if map_tile_change_flag == 0:
        map_tile_index_trans = [0, 0]
        Pre_UAV_info['map_tile_index'] = map_tile_index


    #     # 不找全部地图，只找附近的9块地图（3*3）
    #     lon_length_list = []
    #     lat_length_list = []
    #     delta = [-3, -2, -1, 0, 1, 2, 3]
    #     # map_tile_row = delta + pre_map_tile_row
    #     map_tile_row = [n + pre_map_tile_row for n in delta]
    #     # map_tile_col = delta + pre_map_tile_col
    #     map_tile_col = [n + pre_map_tile_col for n in delta]
    #     for i in map_tile_row:      # 确定排查的三排都在地图切片棋盘格之内
    #         if i >= map_tile_row_amount or i < 0:
    #             map_tile_row.remove(i)
    #     for i in map_tile_col:
    #         if i >= map_tile_col_amount or i < 0:
    #             map_tile_col.remove(i)

    #     for row_i in map_tile_row:
    #         map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
    #         map_tile_lat_mid = map_tile_info.lat_mid
    #         lat_length = abs(map_tile_lat_mid - pic_lon_init)
    #         lat_length_list.append(lat_length)
    #     delta_row_index = lat_length_list.index(min(lat_length_list))
    #     lat_index_nearest = map_tile_row[delta_row_index]
    #     for col_i in map_tile_col:
    #         map_tile_info = MAP_TILE_GEO_INFO[lat_index_nearest][col_i]
    #         map_tile_lon_mid = map_tile_info.lon_mid
    #         lon_length = abs(map_tile_lon_mid - pic_lon_init)
    #         lon_length_list.append(lon_length)
    #     delta_col_index = lon_length_list.index(min(lon_length_list))
    #     lon_index_nearest = map_tile_col[delta_col_index]
    #     curr_map_tile_index = [lat_index_nearest, lon_index_nearest]
    #     if curr_map_tile_index == pre_map_tile_index:
    #         Pre_UAV_info['map_tile_index'] = pre_map_tile_index
    #     else:
    #         Pre_UAV_info['map_tile_index'] = curr_map_tile_index
    #         map_tile_index_trans = (numpy.array(curr_map_tile_index) - numpy.array(pre_map_tile_index)).tolist()
    #         map_tile_change_flag = 1
    #     pass
    # if map_tile_change_flag == 0:
    #     map_tile_index_trans = [0, 0]
    # # lat_index_nearest = Pre_UAV_info['map_tile_index'][0]
    # # lon_index_nearest = Pre_UAV_info['map_tile_index'][1]
    # # map_loc = MAP_TILE_GEO_INFO[lat_index_nearest][lon_index_nearest].path

    return Pre_UAV_info, map_tile_index_trans




    
    # 如果小地图变了，这里需要加入一个坐标系转换——邵星雨
    # 理论上在初始化阶段没有变化坐标系的问题
    if map_tile_change_flag == 1:
        curr_P_init_ = curr_P_init
        # 计算坐标系平移的距离
        # 这里应该用像素数做平移的单位吗
        # 用像素需要知道裁剪的时候横向和纵向分别每次各移动多少像素
        coordinate_trans_x = map_tile_index_trans[0] * map_tile.pixel_trans_width
        coordinate_trans_y = map_tile_index_trans[1] * map_tile.pixel_trans_height
        coordinate_trans_H = [1, 0, coordinate_trans_x,
                              0, 1, coordinate_trans_y,
                              0, 0, 1]
        
        H_standard_np_inv = np.array(coordinate_trans_H).reshape((3,3))

        # 这里应该得少求一个逆，但是我不确定应该少求哪个逆？——邵星雨（不确定）

        H_standard_np = np.linalg.inv(H_standard_np_inv)    # 求逆
        P_standard_np = np.reshape(H_standard_np - np.eye(3), (9, 1))

        # P_standard_np = np.reshape(H_standard_np_inv - np.eye(3), (9, 1))
        P_standard = np.expand_dims(P_standard_np[:8], 0)


        H_all_inv = param_utility.p_to_H(P_standard)

        H_all = H_all_inv
        # H_all = np.linalg.inv(H_all_inv)
        
        coordinate_trans_P = param_utility.H_to_p(H_all)

        curr_P_init = compute_Pmk(curr_P_init_, coordinate_trans_P, T)








        # flag_found_map_tile = 0
        # for row_i in range(map_tile_row_amount):
        #     map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
        #     map_tile_lat_1 = map_tile_info.lat_range[0]
        #     map_tile_lat_2 = map_tile_info.lat_range[1]
        #     flag_lat = (pic_lat_init - map_tile_lat_1) * (pic_lat_init - map_tile_lat_2)
          
        #     if flag_lat < 0:
        #         for col_i in range(map_tile_col_amount):
        #             map_tile_info = MAP_TILE_GEO_INFO[row_i][col_i]
        #             map_tile_lon_1 = map_tile_info.lon_range[0]
        #             map_tile_lon_2 = map_tile_info.lon_range[1]
        #             flag_lon = (pic_lon_init - map_tile_lon_1) * (pic_lon_init - map_tile_lon_2)
        #             if flag_lon < 0:    # 判断第一张图片的经纬度在不在当前地图切片的范围之内，如果在，就停止循环
        #                 flag_found_map_tile = 1
        #                 break
        #             else:
        #                 pass
        #         break
        #     else:
        #         pass
        
        # # 如果有一种照片跨了两个地图切片，应当找中心点坐标最接近的那一个地图切片
        # if flag_found_map_tile == 0:
        #     mid_distance_list = []
        #     for map_tile_i in range(len(map_tile_list)):
        #         map_tile_info = map_tile_info_list[map_tile_i]
        #         pic_coordinate = [pic_lon_init, pic_lat_init]
        #         map_tile_coordinate = [map_tile_info.mid_lon, map_tile_info.mid_lat]
        #         mid_distance = math.dist(pic_coordinate, map_tile_coordinate)
        #         mid_distance_list.append(mid_distance)
        #     index_nearest = mid_distance_list.index(min(mid_distance_list))
        #     map_tile_path = map_tile_info_list[index_nearest].path  # 找到与当前照片最小距离的地图的路径# flag_found_map_tile = 0
        # for row_i in range(map_tile_row_amount):
        #     map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
        #     map_tile_lat_1 = map_tile_info.lat_range[0]
        #     map_tile_lat_2 = map_tile_info.lat_range[1]
        #     flag_lat = (pic_lat_init - map_tile_lat_1) * (pic_lat_init - map_tile_lat_2)
          
        #     if flag_lat < 0:
        #         for col_i in range(map_tile_col_amount):
        #             map_tile_info = MAP_TILE_GEO_INFO[row_i][col_i]
        #             map_tile_lon_1 = map_tile_info.lon_range[0]
        #             map_tile_lon_2 = map_tile_info.lon_range[1]
        #             flag_lon = (pic_lon_init - map_tile_lon_1) * (pic_lon_init - map_tile_lon_2)
        #             if flag_lon < 0:    # 判断第一张图片的经纬度在不在当前地图切片的范围之内，如果在，就停止循环
        #                 flag_found_map_tile = 1
        #                 break
        #             else:
        #                 pass
        #         break
        #     else:
        #         pass
        
        # # 如果有一种照片跨了两个地图切片，应当找中心点坐标最接近的那一个地图切片
        # if flag_found_map_tile == 0:
        #     mid_distance_list = []
        #     for map_tile_i in range(len(map_tile_list)):
        #         map_tile_info = map_tile_info_list[map_tile_i]
        #         pic_coordinate = [pic_lon_init, pic_lat_init]
        #         map_tile_coordinate = [map_tile_info.mid_lon, map_tile_info.mid_lat]
        #         mid_distance = math.dist(pic_coordinate, map_tile_coordinate)
        #         mid_distance_list.append(mid_distance)
        #     index_nearest = mid_distance_list.index(min(mid_distance_list))
        #     map_tile_path = map_tile_info_list[index_nearest].path  # 找到与当前照片最小距离的地图的路径