import cv2
import numpy as np
import os


def VideoWrite():
    VideoFPS = 10  # 认为其原图象为3帧
    ImgPath = 'H:\\Dataset\\camera1\\'
    VideoPath = 'H:\\Dataset'
    VideoPath = VideoPath + "/Cheetah_FPS_" + str(VideoFPS) + ".mp4"
    VideoSize = (2048, 1536)
    VideoFourcc = cv2.VideoWriter.fourcc(*"mp4v")
    VideoWriter = cv2.VideoWriter('2output.mp4', VideoFourcc, VideoFPS, VideoSize)
    for FrameIndx, VideoFrames in enumerate(os.listdir(ImgPath)):
        print("finish", FrameIndx)
        VideoFrames = str(FrameIndx * 8 + 600).zfill(6) + '.jpg'
        if (FrameIndx % (10 / VideoFPS) == 0):
            ImgSinglePath = ImgPath + VideoFrames
            img = cv2.imread(ImgSinglePath)
            # cv2.imshow('1',img)
            # cv2.waitKey(50)
            VideoWriter.write(img)
        if (FrameIndx > 150):
            break


if __name__ == "__main__":
    a = cv2.imread('Warp_610.png')     # warp_610是指扭曲后的函数吗？
    b = cv2.imread('result_610.png')
    c = np.concatenate((a, b[:, 160:480]), axis=1)
    pass