import glob
from os import listdir
import os
import copy
from pickle import FALSE

from matplotlib.scale import scale_factory
import utility.config as config
import haversine
import numpy
import cv2
from fractions import Fraction

from utility.config import img_height_opt_net, img_height_rel_pose

UAV_pic_width_meters = 150  # 实时图的地理宽度

# %% 地图类
class Map_Tile_Info:
    def __init__(self, path: str, lon_list: list[float], lat_list: list[float]):
        # 路径，lon经度（0,180），lat纬度（0,90）
        self.path = path       # 地图切片路径
        self.lon_range = lon_list      # 经纬度范围
        self.lat_range = lat_list
        self.lon_mid = (lon_list[0] + lon_list[1]) / 2    # 求中心点坐标
        self.lat_mid = (lat_list[0] + lat_list[1]) / 2

# @左上经度@左上纬度@右下经度@右下纬度
# 正好符合上北下南左西右东

# %% 自定义参数

# # 每个地图切片的像素宽度。每个地图切片都是正方形所以宽和高相等
# map_tile_lenghth = 500    # 单位是像素
# # 切出来是棋盘格正方形


generate_new_map_tile = False       # TODO: 要不要重新生成地图切片

target_w = 600                  # TODO: set the width corresponding to the shape of your query image
target_h = 600                  # TODO: set the width corresponding to the shape of your query image

w_h_factor = target_h / target_w

# map_patch_meters = 320        # TODO: set the meters in width the you want to crop

map_tile_width_meters = 300     # TODO: set the meters in width the you want to crop
# 这是指地图切片的宽度对应到地面上是多长（米为单位）
map_tile_heigth_meters = map_tile_width_meters * w_h_factor


stride_ratio_str = '1/4'              # TODO: 确定每次切割平移的步幅是该维度切片长度的几倍
numerator = int(stride_ratio_str.split('/')[0])
denominator = int(stride_ratio_str.split('/')[1])
stride_ratio = float(Fraction(numerator, denominator))


# geo_trans_width = 50          # 单位是米
# geo_trans_height = 50

geo_trans_width = stride_ratio * map_tile_width_meters       # 单位是米
geo_trans_height = stride_ratio * map_tile_width_meters * w_h_factor

unit_pixel_geo_length = map_tile_width_meters / target_w    # 直接用设定时的数据求地图切片的每个像素代表多少米


map_path = config.map_loc
# map_width = config.map_width
# map_height = config.map_height
map_GPS_1 = config.Map_GPS_1
map_GPS_2 = config.Map_GPS_2
original_scale = config.init_scale
# map_resolution = config.map_resolution

map_dir = map_path
map_data = cv2.imread(map_dir)

map_w = map_data.shape[1]   # 大地图像素宽度
map_h = map_data.shape[0]   # 大地图像素高度

gnss_data = map_dir.split('\\')[-1]

try:
    LT_lon = float(gnss_data.split('@')[2]) # left top 左上
    LT_lat = float(gnss_data.split('@')[3])
    RB_lon = float(gnss_data.split('@')[4]) # right bottom 右下
    RB_lat = float(gnss_data.split('@')[5])
except Exception:
    LT_lon = map_GPS_1[0]
    LT_lat = map_GPS_1[1]
    RB_lon = map_GPS_2[0]
    RB_lat = map_GPS_2[1]

lon_res = (RB_lon - LT_lon) / map_w     # 大地图的纬线方向每像素代表的经度跨度
lat_res = (RB_lat - LT_lat) / map_h     # 大地图的经线方向每像素代表的纬度跨度

map_width_meters = haversine.haversine([RB_lat, RB_lon], [RB_lat, LT_lon], unit = haversine.Unit.METERS)
map_height_meters = haversine.haversine([RB_lat, RB_lon], [LT_lat, RB_lon], unit = haversine.Unit.METERS)
pixel_per_meter_factor = ((map_w / map_width_meters) + (map_h / map_height_meters)) / 2     # 得出来是像素/米


stride_x = round(pixel_per_meter_factor * map_tile_width_meters * stride_ratio)
stride_y = round(pixel_per_meter_factor * map_tile_width_meters * stride_ratio * w_h_factor)

pixel_trans_width = stride_x
pixel_trans_height = stride_y

img_w = round(pixel_per_meter_factor * map_tile_width_meters)              # 求地图切片在原始地图上的宽度（像素为单位）
img_h = round(pixel_per_meter_factor * map_tile_width_meters * w_h_factor) # 求地图切片在原始地图上的高度（像素为单位）

# initial_scale = UAV_pic_width_meters / map_tile_heigth_meters
# initial_scale = UAV_pic_width_meters / map_tile_width_meters # FIXME - 加个/1.25
# initial_scale = UAV_pic_width_meters / map_tile_width_meters / 1.25 # FIXME
initial_scale = UAV_pic_width_meters / map_tile_width_meters * 1.10 # FIXME

# 地图切片的存储路径
# map_tile_loc = 'E:\\GeoVINS\\AerialVL\\images\\VPR\\map_database\\180\\'
frac_idx = stride_ratio_str.replace('/','-')
if 'geo_referenced_map_tile' in map_path:
    map_tile_loc = 'E:\\GeoVINS\\AerialVL\\images\\VAL\\geo_referenced_map_tile\\wm, w, h, stride_%d, %d, %d, %s\\' %(map_tile_width_meters, target_w, target_h, frac_idx)
else:
    map_tile_loc = map_path.replace(map_path.split('\\')[-1],'') + 'geo_referenced_map_tile\\wm, w, h, stride_%d, %d, %d, %s\\' %(map_tile_width_meters, target_w, target_h, frac_idx)

result_save_idx = 'wm_w_h_stride~~%d_%d_%d_%s' %(map_tile_width_meters, target_w, target_h, frac_idx) # 给主程序存储结果文件使用

if not os.path.exists(map_tile_loc):
    os.makedirs(map_tile_loc)
    generate_new_map_tile = True


if generate_new_map_tile:

    patches_save_dir = map_tile_loc
    
    loc_x = 0
    while loc_x < map_w - img_w:    # 已分割像素宽度<大地图宽度-地图切片宽度
        loc_y = 0
        while loc_y < map_h - img_h:
            LT_cur_lon = str(loc_x * lon_res + LT_lon)
            LT_cur_lat = str(loc_y * lat_res + LT_lat)
            RB_cur_lon = str((loc_x + img_w) * lon_res + LT_lon)
            RB_cur_lat = str((loc_y + img_h) * lat_res + LT_lat)
            CT_cur_lon = str((loc_x + img_w / 2) * lon_res + LT_lon)    # centre
            CT_cur_lat = str((loc_y + img_h / 2) * lat_res + LT_lat)
            img_seg_pad = map_data[loc_y:loc_y + img_h, loc_x:loc_x + img_w]
            img_seg_pad = cv2.resize(img_seg_pad, (target_w, target_h), interpolation = cv2.INTER_LINEAR) 

            # 决定是否要旋转
            # img_seg_pad = numpy.clip(numpy.rot90(img_seg_pad, 1), 0, 255).astype(numpy.uint8)  # rotate if necessary

            cv2.imwrite(patches_save_dir + '@map%s.png' % (
                    '@' + LT_cur_lon + '@' + LT_cur_lat + '@' + RB_cur_lon + '@' + RB_cur_lat + '@'), img_seg_pad)
            # cv2.imwrite(patches_save_dir + '%s.png' % (
            #         '@' + CT_cur_lon + '@' + CT_cur_lat + '@'), img_seg_pad)
            print('%s.png' % ('@' + LT_cur_lon + '@' + LT_cur_lat + '@' + RB_cur_lon + '@' + RB_cur_lat + '@'))

            loc_y = loc_y + stride_y

        loc_x = loc_x + stride_x
    

# 这里应该加一个能够根据飞行高度切割地图块的函数
# 需要保证必须按照经线和纬线切，类似棋盘格
MAP_TILE_GEO_INFO = []      # 这里我想记录一个包含了地理相对位置的地图切片列表，能够找到某一图块东西南北相邻的地图切片
# map_tile_list = sorted(glob.glob(map_tile_loc))
map_tile_list = []
for filename in listdir(map_tile_loc):
    map_tile_list.append(map_tile_loc + filename)   # 得到绝对路径
map_tile_amount = len(map_tile_list)
flag_found_map_tile = 0
map_tile_info_list = []
for map_tile_i in range(map_tile_amount):
    map_tile_path = map_tile_list[map_tile_i]
    map_tile_lon_1 = float(map_tile_path.split('@')[2])
    map_tile_lat_1 = float(map_tile_path.split('@')[3])
    map_tile_lon_2 = float(map_tile_path.split('@')[4])
    map_tile_lat_2 = float(map_tile_path.split('@')[5])
    map_tile_lon = [map_tile_lon_1, map_tile_lon_2]     # longitude经度范围
    map_tile_lat = [map_tile_lat_1, map_tile_lat_2]     # latitude纬度范围

    map_tile_info = Map_Tile_Info(map_tile_path, map_tile_lon, map_tile_lat)
    # map_tile_info_list.append(map_tile_info)        # 这里append会覆盖前面所有元素！为啥？
    # map_tile_info_list.append(copy.deepcopy(map_tile_info))
    map_tile_info_list.append(copy.copy(map_tile_info))

map_tile_info_list_tmp = map_tile_info_list
map_tile_col_amount = 1 # 在地图中的row和col，每一行的经度是相同的
map_tile_info_list = sorted(map_tile_info_list_tmp, key = lambda info: (-info.lat_mid, info.lon_mid))
del map_tile_info_list_tmp
# 北半球纬度从上往下是逐渐变小的，所以加负号，北半球经度从左往右是逐渐变大的，让二维list里的上下左右符合地球、地图上的上下左右
common_lat = map_tile_info_list[0].lat_mid  # 记下第一个地图切片的中间点的经度，看每一个经度上有多少地图切片
for map_tile_i in range(map_tile_amount):
    if common_lat != map_tile_info_list[map_tile_i].lat_mid:
        map_tile_col_amount = map_tile_i
        break
map_tile_row_amount = round(map_tile_amount / map_tile_col_amount)
for i in range(map_tile_row_amount):
    MAP_TILE_GEO_INFO.append(map_tile_info_list[i * map_tile_col_amount: (i + 1) * map_tile_col_amount])
# 54行  87列

scale_img_to_map = target_h / img_height_rel_pose    # REVIEW - 如果是转了90度的话应该用target_w除

# %% 这里需要计算照片中的一个像素应该对应多少米，每次横向和纵向各移动50米

# # 横向距离
# if map_tile_row_amount % 2 == 0:    # 偶数列
#     mid_1 = round(map_tile_row_amount / 2)
#     mid_2 = mid_1 + 1
#     map_tile_mid_1 = MAP_TILE_GEO_INFO[mid_1][0]
#     map_tile_mid_2 = MAP_TILE_GEO_INFO[mid_2][0]
#     lat_mid = (map_tile_mid_1.lat_mid + map_tile_mid_2.lat_mid) / 2

# else:
#     mid = round((map_tile_row_amount + 1) / 2)
#     map_tile_mid = MAP_TILE_GEO_INFO[mid][0]
#     lat_mid = map_tile_mid.lat_mid

# w_1 = lat_mid
# w_2 = MAP_TILE_GEO_INFO[0][0].lon_range
# # (lat, lon)
# width_coordinate_1 = (w_1, w_2[0])
# width_coordinate_2 = (w_1, w_2[1])
# map_tile_geo_width = haversine.haversine(width_coordinate_1, width_coordinate_2, haversine.Unit.METERS)

# # 纵向距离
# h_1 = MAP_TILE_GEO_INFO[0][0].lat_range
# h_2 = MAP_TILE_GEO_INFO[0][0].lon_mid
# # (lat, lon)
# height_coordinate_1 = (h_1[0], h_2)
# height_coordinate_2 = (h_1[1], h_2)
# map_tile_geo_height = haversine.haversine(height_coordinate_1, height_coordinate_2, haversine.Unit.METERS)



# map_tile_geo_length = (map_tile_geo_height + map_tile_geo_width) / 2        # 这里求个平均，理论上这个宽和高应该是相等的


# 求每张地图一个像素的宽度对应多少米

# unit_pixel_geo_length = map_tile_geo_length / map_tile_lenghth      # 地图切片中一个像素对应的地表的长度


# pixel_trans_width = round(geo_trans_width / unit_pixel_geo_length)  # 求平移的像素数，不过目前横向和纵向平移的像素数应该是一样的。
# pixel_trans_height = round(geo_trans_height / unit_pixel_geo_length)


# %% 求地图切片的分辨率和scale
map_tile_resolution = unit_pixel_geo_length                             
# initial_scale = original_scale * map_resolution / map_tile_resolution   # 大地图scale*大地图分辨率/小地图分辨率

# 照片分辨率/地图分辨率
# initial_scale = original_scale * 19.07       # 乘19.07试试
# initial_scale = original_scale * 19.04       # 乘19.04试试
# initial_scale = original_scale * map_scale_factory

# %% 求一下大地图横向和纵向跨越的距离
# map_lon_1 = map_GPS_1[0]
# map_lon_2 = map_GPS_2[0]
# map_lat_1 = map_GPS_1[1]
# map_lat_2 = map_GPS_2[1]
# original_map_info = Map_Tile_Info(map_path, [map_lon_1, map_lon_2], [map_lat_1, map_lat_2])

# # 横向距离（经度longitude跨度）
# w_1 = original_map_info.lat_mid
# w_2 = original_map_info.lon_range
# # (lat, lon)
# width_coordinate_1 = (w_1, w_2[0])
# width_coordinate_2 = (w_1, w_2[1])

# original_map_geo_width = haversine.haversine(width_coordinate_1, width_coordinate_2, haversine.Unit.METERS)

# # 纵向距离（纬度latitude跨度）
# h_1 = original_map_info.lat_range
# h_2 = original_map_info.lon_mid
# # (lat, lon)
# height_coordinate_1 = (h_1[0], h_2)
# height_coordinate_2 = (h_1[1], h_2)

# original_map_geo_height = haversine.haversine(height_coordinate_1, height_coordinate_2, haversine.Unit.METERS)

# %% 函数定义

def choose_map_tile(Pre_UAV_info: dict):
    map_tile_change_flag = 0

    if Pre_UAV_info['map_tile_index'] == [-1, -1]:# 只有初始化的时候才是[-1, -1]

        pic_lon_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[2])
        pic_lat_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[3])

        map_tile_index = choose_map_tile_all_search(pic_lon_init, pic_lat_init)
        Pre_UAV_info['map_tile_index'] = map_tile_index
        
    else:   # 不是初始化的情况，也就是已经进入while true循环之后。
        
        # 需要将上一张定位的像素点位置转化为经纬度坐标？
        # 需要记录上一张图片的编号，下一次我们首次测试该编号的地图是否包含第二个图块进行检验；
        # 如果照片不在上一次使用的地图块范围内，则对这个图块四周的8个或者4个地图块进行测试，看这9个或5个地图块哪个最符合。
        pic_GPS = Pre_UAV_info['curr_GPS']
        pic_lon_init = pic_GPS[0]
        pic_lat_init = pic_GPS[1]
        pre_map_tile_index = Pre_UAV_info['map_tile_index']
        
        # 还是要全部地图都找一遍
        map_tile_index = choose_map_tile_all_search(pic_lon_init, pic_lat_init)
        # map_tile_index = choose_map_tile_padding_search(pic_lon_init, pic_lat_init, pre_map_tile_index, 4)
        if map_tile_index == pre_map_tile_index:
            Pre_UAV_info['map_tile_index'] = pre_map_tile_index
        else:
            Pre_UAV_info['map_tile_index'] = map_tile_index
            map_tile_index_trans = (numpy.array(map_tile_index) - numpy.array(pre_map_tile_index)).tolist()
            map_tile_change_flag = 1
        pass
    if map_tile_change_flag == 0:
        map_tile_index_trans = [0, 0]
        Pre_UAV_info['map_tile_index'] = map_tile_index

    return Pre_UAV_info, map_tile_index_trans


def choose_map_tile_all_search(pic_lon_init, pic_lat_init):
    lon_length_list = []
    lat_length_list = []
    for col_i in range(map_tile_col_amount):
        map_tile_info = MAP_TILE_GEO_INFO[0][col_i]
        map_tile_lon_mid = map_tile_info.lon_mid
        lon_length = abs(map_tile_lon_mid - pic_lon_init)
        lon_length_list.append(lon_length)
    lon_index_nearest = lon_length_list.index(min(lon_length_list)) # 横向上找经度longitude最接近的列

    for row_i in range(map_tile_row_amount):
        map_tile_info = MAP_TILE_GEO_INFO[row_i][lon_index_nearest]
        map_tile_lat_mid = map_tile_info.lat_mid

        lat_length = abs(map_tile_lat_mid - pic_lat_init)
        
        # map_tile_lon_mid = map_tile_info.lon_mid
        # point_1 = (map_tile_lat_mid, map_tile_lon_mid)
        # point_2 = (pic_lat_init, pic_lon_init)
        # lat_length = haversine.haversine(point_1, point_2, haversine.Unit.METERS)

        lat_length_list.append(lat_length)
    lat_index_nearest = lat_length_list.index(min(lat_length_list)) # 纵向上找纬度latitude最接近的列
    map_tile_index = [lat_index_nearest, lon_index_nearest]

    return map_tile_index

def choose_map_tile_padding_search(pic_lon_init, pic_lat_init, pre_map_tile_index, search_padding):
    lon_length_list = []
    lat_length_list = []
    lon_index_list = []
    lat_index_list = []
    lon_init_index = pre_map_tile_index[1]
    lat_init_index = pre_map_tile_index[0]
    lon_index_list_init = range(lon_init_index - search_padding, lon_init_index + search_padding + 1, 1)
    lat_index_list_init = range(lat_init_index - search_padding, lat_init_index + search_padding + 1, 1)
    for i in lon_index_list_init:
        if i >= 0 and i < map_tile_col_amount:
            lon_index_list.append(i)
    for i in lat_index_list_init:
        if i >= 0 and i < map_tile_row_amount:
            lat_index_list.append(i)
    for col in lon_index_list:
        map_tile_info = MAP_TILE_GEO_INFO[0][col]
        map_tile_lon_mid = map_tile_info.lon_mid
        lon_length = abs(map_tile_lon_mid - pic_lon_init)
        lon_length_list.append(lon_length)
    lon_index_nearest = lon_index_list[lon_length_list.index(min(lon_length_list))] # 横向上找经度longitude最接近的列
    for row in lat_index_list:
        map_tile_info = MAP_TILE_GEO_INFO[row][lon_index_nearest]
        map_tile_lat_mid = map_tile_info.lat_mid
        lat_length = abs(map_tile_lat_mid - pic_lat_init)
        lat_length_list.append(lat_length)
    lat_index_nearest = lat_index_list[lat_length_list.index(min(lat_length_list))] # 纵向上找纬度latitude最接近的列
    map_tile_index = [lat_index_nearest, lon_index_nearest]

    return map_tile_index


# def choose_map_tile(Pre_UAV_info: dict):
#     map_tile_change_flag = 0

#     if Pre_UAV_info['map_tile_index'] == [-1, -1]:# 只有初始化的时候才是[-1, -1]

#         pic_lon_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[2])
#         pic_lat_init = float((Pre_UAV_info['image_list'][Pre_UAV_info['image_idx']]).split('@')[3])

#         lon_length_list = []
#         lat_length_list = []
#         for col_i in range(map_tile_col_amount):
#             map_tile_info = MAP_TILE_GEO_INFO[0][col_i]
#             map_tile_lon_mid = map_tile_info.lon_mid
#             lon_length = abs(map_tile_lon_mid - pic_lon_init)
#             lon_length_list.append(lon_length)
#         lon_index_nearest = lon_length_list.index(min(lon_length_list)) # 横向上找经度longitude最接近的列

#         for row_i in range(map_tile_row_amount):
#             map_tile_info = MAP_TILE_GEO_INFO[row_i][lon_index_nearest]
#             map_tile_lat_mid = map_tile_info.lat_mid
#             # lat_length = abs(map_tile_lat_mid - pic_lat_init)
            
#             map_tile_lon_mid = map_tile_info.lon_mid
#             point_1 = (map_tile_lat_mid, map_tile_lon_mid)
#             point_2 = (pic_lat_init, pic_lon_init)
#             lat_length = haversine.haversine(point_1, point_2, haversine.Unit.METERS)

#             lat_length_list.append(lat_length)
#         lat_index_nearest = lat_length_list.index(min(lat_length_list)) # 纵向上找纬度latitude最接近的列
#         map_tile_index = [lat_index_nearest, lon_index_nearest]
#         Pre_UAV_info['map_tile_index'] = map_tile_index
        
#     else:   # 不是初始化的情况，也就是已经进入while true循环之后。
        
#         # 需要将上一张定位的像素点位置转化为经纬度坐标？
#         # 需要记录上一张图片的编号，下一次我们首次测试该编号的地图是否包含第二个图块进行检验；
#         # 如果照片不在上一次使用的地图块范围内，则对这个图块四周的8个或者4个地图块进行测试，看这9个或5个地图块哪个最符合。
#         pic_GPS = Pre_UAV_info['curr_GPS']
#         pic_lon_init = pic_GPS[0]
#         pic_lat_init = pic_GPS[1]
#         pre_map_tile_index = Pre_UAV_info['map_tile_index']
#         # pre_map_tile_row = pre_map_tile_index[0]
#         # pre_map_tile_col = pre_map_tile_index[1]
#         # pre_map_tile_info = MAP_TILE_GEO_INFO[pre_map_tile_row][pre_map_tile_col]
#         # pre_map_tile_lat_1 = pre_map_tile_info.lat_range[0]
#         # pre_map_tile_lat_2 = pre_map_tile_info.lat_range[1]
#         # pre_map_tile_lon_1 = pre_map_tile_info.lon_range[0]
#         # pre_map_tile_lon_2 = pre_map_tile_info.lon_range[1]
#         # lat_flag = (pre_map_tile_lat_1 - pic_lat_init) * (pre_map_tile_lat_2 - pic_lat_init)
#         # lon_flag = (pre_map_tile_lon_1 - pic_lon_init) * (pre_map_tile_lon_2 - pic_lon_init)
#         # if lat_flag < 0 and lon_flag < 0:
#         #     Pre_UAV_info['map_tile_index'] = pre_map_tile_index
#         # else:
#         #     lon_length_list = []
#         #     lat_length_list = []
#         #     delta = [-1, 0, 1]
#         #     map_tile_row = delta + pre_map_tile_row
#         #     map_tile_col = delta + pre_map_tile_col
#         #     for i in map_tile_row:      # 确定排查的三排都在地图切片棋盘格之内
#         #         if i >= map_tile_row_amount or i < 0:
#         #             map_tile_row.remove(i)
#         #     for i in map_tile_col:
#         #         if i >= map_tile_col_amount or i < 0:
#         #             map_tile_col.remove(i)

#         #     for row_i in map_tile_row:
#         #         map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
#         #         map_tile_lon_mid = map_tile_info.lon_mid
#         #         lon_length = abs(map_tile_lon_mid - pic_lon_init)
#         #         lon_length_list.append(lon_length)
#         #     lon_index_nearest = lon_length_list.index(min(lon_length_list))
#         #     for col_i in map_tile_col:
#         #         map_tile_info = MAP_TILE_GEO_INFO[lon_index_nearest][col_i]
#         #         map_tile_lat_mid = map_tile_info.lat_mid
#         #         lat_length = abs(map_tile_lat_mid - pic_lat_init)
#         #         lat_length_list.append(lat_length)
#         #     lat_index_nearest = lat_length_list.index(min(lat_length_list))
#         #     curr_map_tile_index = [lat_index_nearest, lon_index_nearest]
#         #     if curr_map_tile_index == pre_map_tile_index:
#         #         Pre_UAV_info['map_tile_index'] = pre_map_tile_index
#         #     else:
#         #         Pre_UAV_info['map_tile_index'] = curr_map_tile_index
#         #         map_tile_index_trans = curr_map_tile_index - pre_map_tile_index
#         #         map_tile_change_flag = 1
        
#         # 还是要全部地图都找一遍
#         lon_length_list = []
#         lat_length_list = []
#         for col_i in range(map_tile_col_amount):
#             map_tile_info = MAP_TILE_GEO_INFO[0][col_i]
#             map_tile_lon_mid = map_tile_info.lon_mid
#             lon_length = abs(map_tile_lon_mid - pic_lon_init)
#             lon_length_list.append(lon_length)
#         lon_index_nearest = lon_length_list.index(min(lon_length_list)) # 横向上找经度longitude最接近的列

#         for row_i in range(map_tile_row_amount):
#             map_tile_info = MAP_TILE_GEO_INFO[row_i][lon_index_nearest]
#             map_tile_lat_mid = map_tile_info.lat_mid
#             # lat_length = abs(map_tile_lat_mid - pic_lat_init)
            
#             map_tile_lon_mid = map_tile_info.lon_mid
#             point_1 = (map_tile_lat_mid, map_tile_lon_mid)
#             point_2 = (pic_lat_init, pic_lon_init)
#             lat_length = haversine.haversine(point_1, point_2, haversine.Unit.METERS)

#             lat_length_list.append(lat_length)
#         lat_index_nearest = lat_length_list.index(min(lat_length_list)) # 纵向上找纬度latitude最接近的列
#         map_tile_index = [lat_index_nearest, lon_index_nearest]
#         if map_tile_index == pre_map_tile_index:
#             Pre_UAV_info['map_tile_index'] = pre_map_tile_index
#         else:
#             Pre_UAV_info['map_tile_index'] = map_tile_index
#             map_tile_index_trans = (numpy.array(map_tile_index) - numpy.array(pre_map_tile_index)).tolist()
#             map_tile_change_flag = 1
#         pass
#     if map_tile_change_flag == 0:
#         map_tile_index_trans = [0, 0]
#         Pre_UAV_info['map_tile_index'] = map_tile_index


#     #     # 不找全部地图，只找附近的9块地图（3*3）
#     #     lon_length_list = []
#     #     lat_length_list = []
#     #     delta = [-3, -2, -1, 0, 1, 2, 3]
#     #     # map_tile_row = delta + pre_map_tile_row
#     #     map_tile_row = [n + pre_map_tile_row for n in delta]
#     #     # map_tile_col = delta + pre_map_tile_col
#     #     map_tile_col = [n + pre_map_tile_col for n in delta]
#     #     for i in map_tile_row:      # 确定排查的三排都在地图切片棋盘格之内
#     #         if i >= map_tile_row_amount or i < 0:
#     #             map_tile_row.remove(i)
#     #     for i in map_tile_col:
#     #         if i >= map_tile_col_amount or i < 0:
#     #             map_tile_col.remove(i)

#     #     for row_i in map_tile_row:
#     #         map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
#     #         map_tile_lat_mid = map_tile_info.lat_mid
#     #         lat_length = abs(map_tile_lat_mid - pic_lon_init)
#     #         lat_length_list.append(lat_length)
#     #     delta_row_index = lat_length_list.index(min(lat_length_list))
#     #     lat_index_nearest = map_tile_row[delta_row_index]
#     #     for col_i in map_tile_col:
#     #         map_tile_info = MAP_TILE_GEO_INFO[lat_index_nearest][col_i]
#     #         map_tile_lon_mid = map_tile_info.lon_mid
#     #         lon_length = abs(map_tile_lon_mid - pic_lon_init)
#     #         lon_length_list.append(lon_length)
#     #     delta_col_index = lon_length_list.index(min(lon_length_list))
#     #     lon_index_nearest = map_tile_col[delta_col_index]
#     #     curr_map_tile_index = [lat_index_nearest, lon_index_nearest]
#     #     if curr_map_tile_index == pre_map_tile_index:
#     #         Pre_UAV_info['map_tile_index'] = pre_map_tile_index
#     #     else:
#     #         Pre_UAV_info['map_tile_index'] = curr_map_tile_index
#     #         map_tile_index_trans = (numpy.array(curr_map_tile_index) - numpy.array(pre_map_tile_index)).tolist()
#     #         map_tile_change_flag = 1
#     #     pass
#     # if map_tile_change_flag == 0:
#     #     map_tile_index_trans = [0, 0]
#     # # lat_index_nearest = Pre_UAV_info['map_tile_index'][0]
#     # # lon_index_nearest = Pre_UAV_info['map_tile_index'][1]
#     # # map_loc = MAP_TILE_GEO_INFO[lat_index_nearest][lon_index_nearest].path

#     return Pre_UAV_info, map_tile_index_trans




        # flag_found_map_tile = 0
        # for row_i in range(map_tile_row_amount):
        #     map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
        #     map_tile_lat_1 = map_tile_info.lat_range[0]
        #     map_tile_lat_2 = map_tile_info.lat_range[1]
        #     flag_lat = (pic_lat_init - map_tile_lat_1) * (pic_lat_init - map_tile_lat_2)
          
        #     if flag_lat < 0:
        #         for col_i in range(map_tile_col_amount):
        #             map_tile_info = MAP_TILE_GEO_INFO[row_i][col_i]
        #             map_tile_lon_1 = map_tile_info.lon_range[0]
        #             map_tile_lon_2 = map_tile_info.lon_range[1]
        #             flag_lon = (pic_lon_init - map_tile_lon_1) * (pic_lon_init - map_tile_lon_2)
        #             if flag_lon < 0:    # 判断第一张图片的经纬度在不在当前地图切片的范围之内，如果在，就停止循环
        #                 flag_found_map_tile = 1
        #                 break
        #             else:
        #                 pass
        #         break
        #     else:
        #         pass
        
        # # 如果有一种照片跨了两个地图切片，应当找中心点坐标最接近的那一个地图切片
        # if flag_found_map_tile == 0:
        #     mid_distance_list = []
        #     for map_tile_i in range(len(map_tile_list)):
        #         map_tile_info = map_tile_info_list[map_tile_i]
        #         pic_coordinate = [pic_lon_init, pic_lat_init]
        #         map_tile_coordinate = [map_tile_info.mid_lon, map_tile_info.mid_lat]
        #         mid_distance = math.dist(pic_coordinate, map_tile_coordinate)
        #         mid_distance_list.append(mid_distance)
        #     index_nearest = mid_distance_list.index(min(mid_distance_list))
        #     map_tile_path = map_tile_info_list[index_nearest].path  # 找到与当前照片最小距离的地图的路径# flag_found_map_tile = 0
        # for row_i in range(map_tile_row_amount):
        #     map_tile_info = MAP_TILE_GEO_INFO[row_i][0]
        #     map_tile_lat_1 = map_tile_info.lat_range[0]
        #     map_tile_lat_2 = map_tile_info.lat_range[1]
        #     flag_lat = (pic_lat_init - map_tile_lat_1) * (pic_lat_init - map_tile_lat_2)
          
        #     if flag_lat < 0:
        #         for col_i in range(map_tile_col_amount):
        #             map_tile_info = MAP_TILE_GEO_INFO[row_i][col_i]
        #             map_tile_lon_1 = map_tile_info.lon_range[0]
        #             map_tile_lon_2 = map_tile_info.lon_range[1]
        #             flag_lon = (pic_lon_init - map_tile_lon_1) * (pic_lon_init - map_tile_lon_2)
        #             if flag_lon < 0:    # 判断第一张图片的经纬度在不在当前地图切片的范围之内，如果在，就停止循环
        #                 flag_found_map_tile = 1
        #                 break
        #             else:
        #                 pass
        #         break
        #     else:
        #         pass
        
        # # 如果有一种照片跨了两个地图切片，应当找中心点坐标最接近的那一个地图切片
        # if flag_found_map_tile == 0:
        #     mid_distance_list = []
        #     for map_tile_i in range(len(map_tile_list)):
        #         map_tile_info = map_tile_info_list[map_tile_i]
        #         pic_coordinate = [pic_lon_init, pic_lat_init]
        #         map_tile_coordinate = [map_tile_info.mid_lon, map_tile_info.mid_lat]
        #         mid_distance = math.dist(pic_coordinate, map_tile_coordinate)
        #         mid_distance_list.append(mid_distance)
        #     index_nearest = mid_distance_list.index(min(mid_distance_list))
        #     map_tile_path = map_tile_info_list[index_nearest].path  # 找到与当前照片最小距离的地图的路径