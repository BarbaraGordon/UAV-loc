import cv2
import numpy as np
import PIL.Image as Image
from torchvision import models, transforms
import exifread
import glob
import math
import sys

from utility.config import *

process_num = 1 # TODO - 0是旧的180，1是新的自己切的

if process_num == 0:
    from utility.map_tile_process import MAP_TILE_GEO_INFO, map_tile_length
elif process_num == 1:
    from utility.map_tile_process_ import MAP_TILE_GEO_INFO, target_w, target_h
# from utility.map_tile_process import MAP_TILE_GEO_INFO, map_tile_length
else:
    print('utility.utils.process_num chosen ERROR!')
    sys.exit(3)


# 原来：def lat_lon_to_pix_pos(M, GPS_target):
def lat_lon_to_pix_pos(map_tile_index: list, GPS_target):     # 这里把原来的M改成了map_tile_index

    # 把经纬度转化为像素位置

    # 加载小地图的话需要改每张地图的宽和高

    if process_num == 0:
        h = map_tile_length
        w = map_tile_length

    else:
        h = target_h
        w = target_w

    # h = map_height
    # w = map_width
    
    target_lat = GPS_target[0]
    target_lon = GPS_target[1]

    # 下面的Map_GPS需要改成小地图的GPS——邵星雨
    row = map_tile_index[0]
    col = map_tile_index[1]
    map_tile_info = MAP_TILE_GEO_INFO[row][col]

    map_lat_range = map_tile_info.lat_range
    Map_lat_1 = map_lat_range[0]
    Map_lat_2 = map_lat_range[1]

    map_lon_range = map_tile_info.lon_range
    Map_lon_1 = map_lon_range[0]
    Map_lon_2 = map_lon_range[1]

    # # 贺梦凡原来的
    # Map_lat_1 = Map_GPS_1[1]
    # Map_lat_2 = Map_GPS_2[1]
    # Map_lon_1 = Map_GPS_1[0]
    # Map_lon_2 = Map_GPS_2[0]

    if 'heading_dct' not in locals():
        heading_dct = 'north'
        

    if heading_dct == 'north':  # 数据集的参数，不重要
        # LT ==> WestNorth
        target_pix_pos_x = h * ((target_lat - Map_lat_1) / (Map_lat_2 - Map_lat_1))
        target_pix_pos_y = w * ((target_lon - Map_lon_1) / (Map_lon_2 - Map_lon_1))
    elif heading_dct == 'east': # type: ignore
        # LT ==> EastNorth
        target_pix_pos_y = w * ((target_lat - Map_lat_1) / (Map_lat_2 - Map_lat_1))
        target_pix_pos_x = h * ((target_lon - Map_lon_1) / (Map_lon_2 - Map_lon_1))
    else:
        target_pix_pos_y = None
        target_pix_pos_x = None
    if target_pix_pos_y and target_pix_pos_x:
        pass
    else:
        sys.exit()
    return [round(abs(target_pix_pos_y)), round(abs(target_pix_pos_x))]


def pix_pos_to_lat_lon(map_tile_index: list, cor_target):  # 这里把原来的M改成了map_tile_index
    # 加载小地图的话需要改每张地图的宽和高
    
    # h = map_height
    # w = map_width

    if process_num == 0:
        h = map_tile_length
        w = map_tile_length

    else:
        h = target_h
        w = target_w

    target_x = cor_target[1]
    target_y = cor_target[0]

    # 下面的Map_GPS需要改成小地图的GPS——邵星雨
    row = map_tile_index[0]
    col = map_tile_index[1]
    map_tile_info = MAP_TILE_GEO_INFO[row][col]

    map_lat_range = map_tile_info.lat_range
    Map_lat_1 = map_lat_range[0]
    Map_lat_2 = map_lat_range[1]

    map_lon_range = map_tile_info.lon_range
    Map_lon_1 = map_lon_range[0]
    Map_lon_2 = map_lon_range[1]

    # # 贺梦凡原来的
    # Map_lon_1 = Map_GPS_1[0]
    # Map_lon_2 = Map_GPS_2[0]
    # Map_lat_1 = Map_GPS_1[1]
    # Map_lat_2 = Map_GPS_2[1]

    try:
        heading_dct # type: ignore
    except NameError:
        heading_dct = 'north'

    if heading_dct == 'north':
        # LT ==> WestNorth
        target_lat = (target_x * (Map_lat_2 - Map_lat_1) / h + Map_lat_1)
        target_lon = (target_y * (Map_lon_2 - Map_lon_1) / w + Map_lon_1)
    elif heading_dct == 'east': # type: ignore
        # LT ==> EastNorth
        target_lon = (target_x * (Map_lon_2 - Map_lon_1) / h + Map_lon_1)
        target_lat = (target_y * (Map_lat_2 - Map_lat_1) / w + Map_lat_1)
    else:
        target_lon = None
        target_lat = None

    return [target_lon, target_lat]


if __name__ == "__main__":
    pass
