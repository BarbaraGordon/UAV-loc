import os
vipsbin = r'D:\Environment\vips-dev\bin'
os.environ['PATH'] = os.pathsep.join((vipsbin, os.environ['PATH']))

from PIL import Image
import torch
from torchvision import transforms
import numpy as np
import cv2
import exifread
import pyvips

# 邵星雨加
# 存成中间过程降采样后的照片和地图块


def plt_axis_match_tens(tens):
    tens = tens.cpu()
    np_img = tens.detach().numpy()
    temp = np.swapaxes(np_img, 0, 2)
    temp = np.swapaxes(temp, 0, 1)
    return temp


def plt_axis_match_np(np_img):
    temp = np.swapaxes(np_img, 0, 2)
    temp = np.swapaxes(temp, 0, 1)
    return temp


def Add_M_Marker(M, xy_cor, color="red"):
    if color == "red":
        color_paint = [255, 0, 0]
    elif color == "blue":
        color_paint = [0, 0, 255]
    elif color == "green":
        color_paint = [0, 255, 0]
    else:
        color_paint = [255, 0, 0]
    w, h = M.size
    x_cor = round(xy_cor[1] / 1)
    y_cor = round(xy_cor[0] * 1)
    M_with_marker_np = np.copy(M)
    M_with_marker_np[x_cor - 10:x_cor + 10, y_cor - 10:y_cor + 10, :] = color_paint
    M_with_marker_pil = Image.fromarray(M_with_marker_np)
    return M_with_marker_pil


def Add_M_Line(M, xy_cor_1, xy_cor_2):
    # ax+by=c
    # 感觉会比较难写啊
    w, h = M.size
    x_1 = xy_cor_1[0]
    y_1 = xy_cor_1[1]
    x_2 = xy_cor_2[0]
    y_2 = xy_cor_2[1]
    M_with_marker_np = np.copy(M)
    k = (y_1 - y_2) / (x_1 - x_2)
    b = (x_1 * y_2 - x_2 * y_1) / (x_1 - x_2)
    max_x = max(round(xy_cor_1[0]), round(xy_cor_2[0])) + 10
    min_x = min(round(xy_cor_1[0]), round(xy_cor_2[0])) - 10
    max_y = max(round(xy_cor_1[1]), round(xy_cor_2[1])) + 10
    min_y = min(round(xy_cor_1[1]), round(xy_cor_2[1])) - 10
    for i in range(min_x, max_x):
        for j in range(min_y, max_y):
            if abs(k * i + b - j) < 10:
                M_with_marker_np[j, i, :] = [255, 0, 0]
    M = Image.fromarray(M_with_marker_np)
    return M


def Add_M_Markers_list(M, xy_cor_list, color="red"):
    # 循环添加
    if type(M) is np.ndarray:
        M_pil = transforms.ToPILImage()(torch.from_numpy(M))
    else:
        M_pil = M
    xy_cor_cur = []
    xy_cor_pre = []
    for xy_cor in xy_cor_list:
        xy_cor_cur = xy_cor_pre
        M_pil = Add_M_Marker(M_pil, xy_cor, color=color)
        if xy_cor_pre != []:
            M_pil = Add_M_Line(M_pil, xy_cor_pre, xy_cor_cur)
    return M_pil


def Add_M_Markers_square(M, xy_cor_list, color="red"):
    # 循环添加
    if type(M) is np.ndarray:
        M_pil = transforms.ToPILImage()(torch.from_numpy(M))
    else:
        M_pil = M
    if len(xy_cor_list) != 4:
        print("error, can not draw a square")
        return 0
    M_pil = Add_M_Marker(M_pil, xy_cor_list[0], color=color)
    M_pil = Add_M_Line(M_pil, xy_cor_list[0], xy_cor_list[1])
    M_pil = Add_M_Marker(M_pil, xy_cor_list[1], color=color)
    M_pil = Add_M_Line(M_pil, xy_cor_list[1], xy_cor_list[2])
    M_pil = Add_M_Marker(M_pil, xy_cor_list[2], color=color)
    M_pil = Add_M_Line(M_pil, xy_cor_list[2], xy_cor_list[3])
    M_pil = Add_M_Marker(M_pil, xy_cor_list[3], color=color)
    M_pil = Add_M_Line(M_pil, xy_cor_list[3], xy_cor_list[0])
    return M_pil


def drawMatches(imageA, imageB, kpsA, kpsB, matches, status):
    # initialize the output visualization image
    (hA, wA) = imageA.shape[:2]
    (hB, wB) = imageB.shape[:2]
    vis = np.zeros((max(hA, hB), wA + wB, 3), dtype="uint8")
    vis[0:hA, 0:wA] = imageA
    vis[0:hB, wA:] = imageB

    # loop over the matches
    for match in matches:
        # only process the match if the keypoint was successfully
        # matched
        # draw the match
        trainIdx = match.trainIdx
        queryIdx = match.queryIdx
        ptA = (int((kpsA[queryIdx].pt)[0]), int((kpsA[queryIdx].pt)[1]))
        ptB = (int((kpsB[trainIdx].pt)[0]) + wA, int((kpsB[trainIdx].pt)[1]))
        cv2.line(vis, ptA, ptB, (0, 255, 0), 2)

    # return the visualization
    return vis


def drawMatches_circles(imageA, imageB, kpsA, kpsB, matches, status):
    # initialize the output visualization image
    (hA, wA) = imageA.shape[:2]
    (hB, wB) = imageB.shape[:2]
    vis = np.zeros((max(hA, hB), wA + wB, 3), dtype="uint8")
    vis[0:hA, 0:wA] = imageA
    vis[0:hB, wA:] = imageB

    # loop over the matches
    for match in matches:
        # only process the match if the keypoint was successfully
        # matched
        # draw the match
        trainIdx = match.trainIdx
        queryIdx = match.queryIdx
        ptA = (int((kpsA[queryIdx].pt)[0]), int((kpsA[queryIdx].pt)[1]))
        ptB = (int((kpsB[trainIdx].pt)[0]) + wA, int((kpsB[trainIdx].pt)[1]))
        cv2.line(vis, ptA, ptB, (0, 255, 0), 1)
        cv2.circle(vis, ptA, color=(0, 255, 0), radius=5, thickness=1)
        cv2.circle(vis, ptB, color=(0, 255, 0), radius=5, thickness=1)

    # return the visualization
    return vis


def get_img_log_lat(img_name):
    """"
    获得标准图片中所含有的GPS信息，适用于文章中使用的village数据集以及gravel_pit数据集
    """
    fd = open(img_name, 'rb')
    tags = exifread.process_file(fd) # type: ignore
    fd.close()

    lag = tags.get("GPS GPSLatitude")
    lag_ref = tags.get("GPS GPSLatitudeRef")
    log = tags.get("GPS GPSLongitude")
    log_ref = tags.get("GPS GPSLongitudeRef")

    if lag == None or log == None:
        lag_numberic = None
        log_numberic = None
    else:
        lag = lag.values
        log = log.values
        lag_numberic = float(lag[0]) + float(lag[1] / 60) + float(lag[2]) / 3600
        log_numberic = float(log[0]) + float(log[1] / 60) + float(log[2]) / 3600
    return [lag_numberic, lag_ref, log_numberic, log_ref]


def load_M(map_loc):
    M_pil = Image.open(map_loc)
    M_tens = transforms.ToTensor()(M_pil)  # 什么意思？https://blog.csdn.net/qq_43799400/article/details/127785104
    M = M_tens.numpy()
    return M


def load_single_I(img_fname: str, img_height_rel_pose, img_height_opt_net):

    # 这个函数是做什么用的：用来加载无人机拍摄的待查询图像。
    curr_img = Image.open(img_fname)
    w1, h1 = curr_img.size
    aspect = w1 / h1        # 宽高比
    # scaled_im_rel_width = round(aspect * img_height_rel_pose)
    # # use Image.NEAREST to reduce the time consumption
    # # 缩放以减少计算量
    # curr_img_rel_rz = curr_img.resize((scaled_im_rel_width, img_height_rel_pose), Image.Resampling.NEAREST)
    # curr_img_rel_rz_tens = transforms.ToTensor()(curr_img_rel_rz)

    # curr_img_rel_rz_np = curr_img_rel_rz_tens.numpy()

    # 存储降采样后的图片——邵星雨
    pic_path = img_fname.replace('long_trajtr','downsampling_templates')
    pic_name = img_fname.split('\\')[-1]
    pic_folder_ = pic_path.strip(pic_name) + 'height_'
    pic_folder = pic_folder_ + str(img_height_rel_pose) + '\\'
    pic_path = pic_folder + pic_name
    # origin_folder = img_fname. strip(pic_name)
    if not os.path.exists(pic_folder):
        os.makedirs(pic_folder, exist_ok=True)
    if os.path.exists(pic_path):
        # curr_img_rel_rz_np = cv2.imread(pic_path)
        curr_img_rel_rz = Image.open(pic_path)
        curr_img_rel_rz_tens = transforms.ToTensor()(curr_img_rel_rz)
        curr_img_rel_rz_np = curr_img_rel_rz_tens.numpy()
    else:
        scaled_im_rel_width = round(aspect * img_height_rel_pose)
        curr_img_rel_rz = curr_img.resize((scaled_im_rel_width, img_height_rel_pose), Image.Resampling.NEAREST)
        curr_img_rel_rz_tens = transforms.ToTensor()(curr_img_rel_rz)

        curr_img_rel_rz_np = curr_img_rel_rz_tens.numpy()

        curr_img_rel_rz.save(pic_path)

    # 给照片之间的视觉里程计进行降采样（如果设置的降采样像素数和上面照片和地图的降采样像素不一致）
    if img_height_opt_net != None:
        # scaled_im_match_width = round(aspect * img_height_opt_net)
        # curr_img_match_rz = curr_img.resize((scaled_im_match_width, img_height_opt_net), Image.Resampling.NEAREST)
        # curr_img_match_rz_tens = transforms.ToTensor()(curr_img_match_rz)\
        
        # curr_img_match_rz_np = curr_img_match_rz_tens.numpy()
        
        # 存储降采样后的图片——邵星雨
        pic_folder = pic_folder_ + str(img_height_opt_net) + '\\'
        pic_path = pic_folder + pic_name
        if not os.path.exists(pic_folder):
            os.makedirs(pic_folder, exist_ok=True)
        if os.path.exists(pic_path):
            # curr_img_match_rz_np = cv2.imread(pic_path)
            curr_img_match_rz = Image.open(pic_path)
            curr_img_match_rz_tens = transforms.ToTensor()(curr_img_match_rz)
            curr_img_match_rz_np = curr_img_match_rz_tens.numpy()
        else:
            scaled_im_match_width = round(aspect * img_height_opt_net)
            curr_img_match_rz = curr_img.resize((scaled_im_match_width, img_height_opt_net), Image.Resampling.NEAREST)
            curr_img_match_rz_tens = transforms.ToTensor()(curr_img_match_rz)
            
            curr_img_match_rz_np = curr_img_match_rz_tens.numpy()

            curr_img_match_rz.save(pic_path)

        return curr_img_rel_rz_np, curr_img_match_rz_np
    else:
        return curr_img_rel_rz_np, None


def load_single_I_np(img_fname, img_height_rel_pose, img_height_opt_net, idx):
    # use numpy to resize the input images to see whether it could save run-time
    curr_img = cv2.imread(img_fname)
    h1, w1, _ = curr_img.shape
    aspect = w1 / h1
    scaled_im_rel_width = round(aspect * img_height_rel_pose)
    curr_img_rel_rz = cv2.resize(curr_img, (scaled_im_rel_width, img_height_rel_pose), interpolation=cv2.INTER_LINEAR)
    curr_img_rel_rz = np.swapaxes(curr_img_rel_rz,0,2)
    curr_img_rel_rz = np.swapaxes(curr_img_rel_rz,1,2)


    if img_height_opt_net != None:
        scaled_im_match_width = round(aspect * img_height_opt_net)
        curr_img_match_rz = cv2.resize(curr_img, (scaled_im_match_width, img_height_opt_net),
                                     interpolation = cv2.INTER_LINEAR)
        curr_img_match_rz = np.swapaxes(curr_img_match_rz, 0, 2)
        curr_img_match_rz = np.swapaxes(curr_img_match_rz, 1, 2)
        return curr_img_rel_rz, curr_img_match_rz
    else:
        return curr_img_rel_rz, None


def fast_load_I(fname):
    # 加载基准图这样比较大的图像时比较有用
    image = pyvips.Image.new_from_file(fname, access="sequential", shrink=4)
    mem_img = pyvips.Image.write_to_memory(image) # type: ignore
    imgnp = np.frombuffer(mem_img, dtype=np.uint8).reshape(image.height, image.width, 3) # type: ignore
    return imgnp


def warp_I():
    img = cv2.imread('drone2.jpg')
    img_color = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    rows, cols, ch = img_color.shape

    pts1 = np.float32([[0, 0], [0, rows], [cols, rows], [cols, 0]]) # type: ignore
    pts2 = np.float32([[0, 0], [500, rows], [cols + 500, rows], [cols + 500, 0]]) # type: ignore
    # 这里500是怎么来的？

    H_gt = cv2.getPerspectiveTransform(pts1, pts2)      # type: ignore # 获取透视变换矩阵函数GetPerspectiveTransform

    print(H_gt)

    dst_img = cv2.warpPerspective(img_color, H_gt, (cols, rows))

    # cv2.imshow('1', dst_img)
    dst_img = cv2.cvtColor(dst_img, cv2.COLOR_RGB2BGR)
    cv2.imwrite('drone2_warp.jpg', dst_img)


def load_GPS_curr():
    pass
