import matplotlib.pyplot as plt
import matplotlib.image as rimg
import numpy as np
import math

from sympy import true


def img_ion():
    #打开交互模式
    plt.ion()
    num = np.random.rand(10)
    img = rimg.imread("..\\village\\frames\\IMG_1126.JPG")
    # 可以实现基本的动态画图功能
    for i in range(200):
        #plt.figure()  #不能加这个，因为这个会创建一个新的画布
        plt.subplot(2,2,1)
        plt.imshow(img)
        print(i)
        if (i%2 == 0):
            plt.subplot(2,2,2)
        else:
            plt.subplot(2,2,3)
        plt.bar(range(10),num)
        plt.subplot(2,2,4)
        plt.imshow(img)
        #停顿时间
        plt.pause(0.1)
        #清除当前画布
        plt.clf()
    plt.ioff()


def locate_result_plt():
    GT_cor_result = []
    OP_cor_result = []
    with open("H:\\Experiment Result\\Match loc\\VIdeo\\Suc\\haidian_airport\\result_1\\GT_cor.txt", "r") as f:
        GT_cor_list = f.readlines()  # 读取文件
        for GT_cor in GT_cor_list:
            GT_cor_info = GT_cor.split(' ')
            lag = float(GT_cor_info[0])
            log = float(GT_cor_info[1])
            GT_cor_result.append([log, lag])
    GT_cor_result_np = np.array(GT_cor_result)

    with open("H:\\Experiment Result\\Match loc\\VIdeo\\Suc\\haidian_airport\\result_1\\OP_cor.txt", "r") as f:
        OP_cor_list = f.readlines()  # 读取文件
        for OP_cor in OP_cor_list:
            OP_cor_info = OP_cor.split(' ')
            lag = float(OP_cor_info[0])
            log = float(OP_cor_info[1])
            OP_cor_result.append([log, lag])
    OP_cor_result_np = np.array(OP_cor_result)

    map_rs = 0.465
    # map_rs = 0.950

    error_list = np.sqrt(np.sum(np.abs(GT_cor_result_np-OP_cor_result_np)**2, axis=1))*map_rs
    error_list_gt = np.mean(error_list)
    error = np.sqrt(np.sum(np.mean(np.abs(GT_cor_result_np-OP_cor_result_np), axis=0)**2))*map_rs
    print(error_list_gt)
    print(error)

    GT_cor_result_np_new_1 = GT_cor_result_np[1:,:]
    GT_cor_result_np_new_2 = GT_cor_result_np[0:-1,:]

    GT_cor_result_np_dis = GT_cor_result_np_new_2 - GT_cor_result_np_new_1
    dis = 0
    for i in GT_cor_result_np_dis:
        dis += math.sqrt(i[0]**2 + i[1]**2)
        pass

    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(GT_cor_result_np[:, 1], 2910 - GT_cor_result_np[:, 0], "+", color="g", ms=0.5)
    plt.plot(OP_cor_result_np[:, 1], 2910 - OP_cor_result_np[:, 0], "*", color="r", ms=0.5)
    plt.axis('on')
    plt.grid(true)
    plt.ylim([0, 2500])
    plt.xlim([0, 4500])
    yy = [0, 500, 1000, 1500, 2000, 2500]
    xx = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500]
    ax.set_aspect('equal', adjustable='box')
    plt.xticks(xx, rotation=0, size=8, weight='bold')
    plt.yticks(yy, rotation=0, size=8, weight='bold')
    plt.xlabel('m', fontsize=10, fontweight='bold')
    plt.ylabel('m', fontsize=10, fontweight='bold')
    plt.legend(labels=["Ground true", "Acquired position"], loc="upper right", fontsize=6)


    plt.savefig('../haidian_airport/trajectory-v2.png', dpi=3000, bbox_inches='tight', pad_inches=0.0)
    plt.show()
    plt.cla()


if __name__ == "__main__":
    plt.rc('font', family='Times New Roman')
    locate_result_plt()