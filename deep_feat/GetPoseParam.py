import cv2
import numpy as np
import torch
import PIL.Image as image
from torchvision import models, transforms
import glob
from PIL import Image
import math
import matplotlib.pyplot as plt
from torchvision import transforms

from torch.autograd import Variable
import exifread
import sys

sys.path.append('..')
sys.path.append('../utility')
sys.path.append('../superpoint')
import superpoint.demo_superpoint as superpoint

import deep_feat.DeepLKBatch as dlk
import utility.img_utils as img_utility
from utility.config import *

USE_CUDA = torch.cuda.is_available()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def get_param(img_batch, template_batch, image_sz):
	template = template_batch.data.squeeze(0).cpu().numpy()
	img = img_batch.data.squeeze(0).cpu().numpy()
	template_show = np.swapaxes(template, 0, 2)
	template_show = np.swapaxes(template_show, 1, 0)
	img_show = np.swapaxes(img, 0, 2)
	img_show = np.swapaxes(img_show, 1, 0)
	sc = image_sz / img.shape[1]

	if template.shape[0] == 3:
		template = np.swapaxes(template, 0, 2)
		template = np.swapaxes(template, 0, 1)
		img = np.swapaxes(img, 0, 2)
		img = np.swapaxes(img, 0, 1)

		template = (template * 255).astype('uint8')
		img = (img * 255).astype('uint8')

	# set_trace()

	template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
	img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	# sift = cv2.xfeatures2d.SIFT_create()
	# sift = cv2.xfeatures2d.SURF_create()
	sift = cv2.SIFT_create()
	# sift = cv2.SIFT.create()
	kp1, des1 = sift.detectAndCompute(template_gray, None)
	kp2, des2 = sift.detectAndCompute(img_gray, None)

	if (len(kp1) >= 2) and (len(kp2) >= 2):

		FLANN_INDEX_KDTREE = 1
		index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
		search_params = dict(checks=50)
		flann = cv2.FlannBasedMatcher(index_params, search_params) # type: ignore
		matches = flann.knnMatch(des1, des2, k=2)

		# store all the good matches as per Lowe's ratio test
		good = []
		for m, n in matches:
			if m.distance < 0.85 * n.distance:
				good.append(m)

		src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore
		dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore

		# need normalization ?
		src_pts = src_pts - image_sz / 2
		dst_pts = dst_pts - image_sz / 2

		if (src_pts.size == 0) or (dst_pts.size == 0):
			H_found = np.eye(3)
		else:
			H_found, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
			# print(src_pts.shape)
			# print(dst_pts.shape)
			# H_found, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 3.0)

		if H_found is None:
			H_found = np.eye(3)

	else:
		H_found = np.eye(3)

	# Perspective_img = cv2.warpPerspective(template, H_found, (template.shape[1], template.shape[0]))
	# plt.imshow(Perspective_img)
	# plt.show()

	H = torch.from_numpy(H_found).float()
	I = torch.eye(3, 3)

	p = H - I

	p = p.view(1, 9, 1)
	p = p[:, 0:8, :]
	# p = p * 3

	if torch.cuda.is_available():
		# return Variable(p.cuda())
		return Variable(p)
	else:
		return Variable(p)


def get_param_spp(img_batch, template_batch, image_sz):
	weights_path = './superpoint/superpoint_v1.pth'
	nms_dist = 4
	conf_thresh = 0.015
	nn_thresh = 0.7
	cuda = torch.cuda.is_available()		# 这里由于电脑是amd显卡所以全部在cpu上进行计算，设置为False。

	fe = superpoint.SuperPointFrontend(weights_path, nms_dist, conf_thresh, nn_thresh, cuda)

	template = template_batch.data.squeeze(0).cpu().numpy()
	img = img_batch.data.squeeze(0).cpu().numpy()

	if template.shape[0] == 3:
		template = np.swapaxes(template, 0, 2)
		template = np.swapaxes(template, 0, 1)
		img = np.swapaxes(img, 0, 2)
		img = np.swapaxes(img, 0, 1)

		template = (template * 255).astype('uint8')
		img = (img * 255).astype('uint8')

	template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
	img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	image_1 = (template_gray.astype('float32') / 255.)
	pts1, image_1_descriptors, heatmap1 = fe.run(image_1)
	des1 = np.swapaxes(image_1_descriptors, 0, 1) # type: ignore
	kp1 = []
	pts1 = np.swapaxes(pts1, 0, 1)
	for keypoint in pts1:
		kp1.append(cv2.KeyPoint(x=keypoint[0], y=keypoint[1], size=1))

	image_2 = (img_gray.astype('float32') / 255.)
	pts2, image_2_descriptors, heatmap2 = fe.run(image_2)
	des2 = np.swapaxes(image_2_descriptors, 0, 1) # type: ignore
	kp2 = []
	pts2 = np.swapaxes(pts2, 0, 1)
	for keypoint in pts2:
		kp2.append(cv2.KeyPoint(x=keypoint[0], y=keypoint[1], size=1))

	if (len(kp1) >= 2) and (len(kp2) >= 2):

		FLANN_INDEX_KDTREE = 1
		index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
		search_params = dict(checks=50)
		flann = cv2.FlannBasedMatcher(index_params, search_params) # type: ignore
		matches = flann.knnMatch(des1, des2, k=2)

		# store all the good matches as per Lowe's ratio test
		good = []
		for m, n in matches:
			if m.distance < 0.85 * n.distance:
				good.append(m)

		src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore
		dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore

		# need normalization ?
		src_pts = src_pts - image_sz / 2
		dst_pts = dst_pts - image_sz / 2

		if (src_pts.size == 0) or (dst_pts.size == 0):
			H_found = np.eye(3)
		else:
			H_found, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

		if H_found is None:
			H_found = np.eye(3)

	else:
		H_found = np.eye(3)

	H = torch.from_numpy(H_found).float()
	I = torch.eye(3, 3)

	p = H - I

	p = p.view(1, 9, 1)
	p = p[:, 0:8, :]

	if torch.cuda.is_available():
		# return Variable(p.cuda())
		return Variable(p)
	else:
		return Variable(p)


def motion_param_calculate():
	# save the homography matrix into a .csv file
	I_list, _ = img_utility.load_I(image_dir, ext=image_dir_ext, scaled_im_height=img_h_rel_pose)
	p = []
	P_init = cal_P_init(img_sz=4800)
	p.append(np.array(P_init))
	# p.append(np.zeros([1, 8, 1]))
	for i in range(I_list.shape[0]-1):
		I_1 = I_list[i:i + 1, :, :, :]
		I_2 = I_list[i + 1:i + 2, :, :, :]
		template_batch = Variable(torch.from_numpy(I_1).squeeze(0))
		img_batch = Variable(torch.from_numpy(I_2).squeeze(0))
		tmp = get_param(img_batch, template_batch, img_h_rel_pose)
		p.append(np.array(tmp))
	np.savetxt(motion_param_loc, np.squeeze(np.array(p)), delimiter=',')


if __name__ == "__main__":
	pass