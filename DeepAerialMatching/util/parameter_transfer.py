import torch


def theta_to_H(theta: torch.Tensor) -> torch.Tensor:
    '''
    输入一个标准仿射变换参数矩阵theta，输出单应矩阵。
    '''
    theta = theta.view(-1, 2, 3)
    expand_line = torch.Tensor([0, 0, 1]).view(1, 1, 3).expand(theta.size(dim=0), 1, 3)
    H = torch.cat((theta, expand_line), 1)
    return H

def H_to_theta(H: torch.Tensor) -> torch.Tensor:
    H = H.view(-1, 3, 3)
    theta = H[:, :2, :]
    return theta