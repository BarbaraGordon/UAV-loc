import cv2
import numpy as np
import torch

import superpoint.demo_superpoint as superpoint


def superpoint_single_match(image_1_color, image_2_color):
    temp = np.swapaxes(image_1_color, 0, 2)
    image_1_color = np.swapaxes(temp, 0, 1)
    temp = np.swapaxes(image_2_color, 0, 2)
    image_2_color = np.swapaxes(temp, 0, 1)

    image_1 = cv2.cvtColor((image_1_color*255).astype('uint8'), cv2.COLOR_RGB2GRAY)
    image_2 = cv2.cvtColor((image_2_color*255).astype('uint8'), cv2.COLOR_RGB2GRAY)

    weights_path = './superpoint/superpoint_v1.pth'
    nms_dist = 4
    conf_thresh = 0.015
    nn_thresh = 0.7
    cuda = torch.cuda.is_available()    # 这里由于电脑是amd显卡所以全部在cpu上进行计算，设置为False。

    fe = superpoint.SuperPointFrontend(weights_path, nms_dist, conf_thresh, nn_thresh, cuda)

    image_1 = (image_1.astype('float32') / 255.)
    pts1, image_1_descriptors, heatmap1 = fe.run(image_1)
    image_1_descriptors = np.swapaxes(image_1_descriptors, 0, 1) # type: ignore
    image_1_kpts = []
    pts1 = np.swapaxes(pts1, 0, 1)
    for keypoint in pts1:
        image_1_kpts.append(cv2.KeyPoint(x=keypoint[0], y=keypoint[1], size=1))

    image_2 = (image_2.astype('float32') / 255.)
    pts2, image_2_descriptors, heatmap2 = fe.run(image_2)
    image_2_descriptors = np.swapaxes(image_2_descriptors, 0, 1) # type: ignore
    image_2_kpts = []
    pts2 = np.swapaxes(pts2, 0, 1)
    for keypoint in pts2:
        image_2_kpts.append(cv2.KeyPoint(x=keypoint[0], y=keypoint[1], size=1))

    if (len(image_1_kpts) >= 10) and (len(image_2_kpts) >= 10):
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)
        flann = cv2.FlannBasedMatcher(index_params, search_params) # type: ignore

        matches = flann.knnMatch(image_1_descriptors, image_2_descriptors, k=2)
        good = []
        for m, n in matches:
            if m.distance < 0.70 * n.distance:
                good.append(m)
        if len(good) > 20:
            src_pts = np.float32([image_1_kpts[m.queryIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore
            src_kpts = []
            for keypoint in src_pts:
                src_kpts.append(cv2.KeyPoint(x=keypoint[0, 0], y=keypoint[0, 1], size=1))
            dst_pts = np.float32([image_2_kpts[m.trainIdx].pt for m in good]).reshape(-1, 1, 2) # type: ignore
            dst_kpts = []
            for keypoint in dst_pts:
                dst_kpts.append(cv2.KeyPoint(x=keypoint[0, 0], y=keypoint[0, 1], size=1))
            if (src_pts.size <= 3) or (dst_pts.size <= 3):
                H_found = np.eye(3)
            else:
                H_found, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 4.0)
        else:
            H_found = np.eye(3)
    else:
        H_found = np.eye(3)

    print(H_found)

    H_found_inv = np.linalg.inv(H_found) # type: ignore
    p_found = np.reshape(np.reshape((H_found_inv - np.eye(3)), (9, 1))[:-1, :], (1, 8, 1))
    return p_found